import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReactTouchEvents from "react-touch-events";
import './Welcome.css';


class Welcome extends Component{
  constructor(props){
    super(props);
    this.handleSwipe=this.handleSwipe.bind(this);
    this.arrNum=0;
    this.state = {
      message: "CREATE AND JOIN YOUR LOCAL TENNIS COMMUNITY",
      messArr: ["CREATE AND JOIN YOUR LOCAL TENNIS COMMUNITY", "FIND YOUR LEVELS, PLAY YOUR MATCHES, CHALLENGE YOURSELF.",
      <p style={{textalign: 'center',margin: '0 auto'}}>WIN PRIZES,<br/> GET SPONSORED,<br/> LIKE A PRO.</p>],
      pOne: "fillThis",
      pTwo: "",
      pTree: "",
      messageState: 0
    }
}
fillPoint(i){
  if(i===0){
    this.setState({
      pOne: "fillThis", pTwo: "", pThree: ""
  });}
  else if(i===1){
    this.setState({
      pOne: "", pTwo: "fillThis", pThree: ""
    });
  }else{
    this.setState({
      pOne: "" , pTwo: "", pThree: "fillThis"
    });
  }
}

// real modulo function
mod(n, m) {
  return ((n % m) + m) % m;
}



handleSwipe (direction) {
    switch (direction) {
        case "top": 
        break;
        case "bottom": 
       break;
        case "left": 
          this.arrNum=this.mod((this.arrNum+1),3),
          this.setState({
          message: this.state.messArr[this.arrNum],
        });
          this.fillPoint(this.arrNum);
        break;
        case "right":
          this.arrNum=this.mod((this.arrNum-1),3),
          this.setState({
          message: this.state.messArr[this.arrNum],
        });
          this.fillPoint(this.arrNum);
        break;
        default:
            break;
        }
}
  render() {
    return (
      <div style={{height: window.innerHeight+"px"}} className={"welcomeComp"}>
        <ReactTouchEvents onSwipe={ this.handleSwipe.bind(this) }>
        <div className={"containerWelcome"}>
          <div className="stripe">
            
          </div>
          {/*TODO create component with message and dots as state */}
          <div className="cent-text">
            <h1 className="welcome">{this.state.message}</h1>
          </div>
          
            <div className="loaded">
              <div id="pOne"  className={this.state.pOne}/>
              <div id="pTwo" className={this.state.pTwo}/>
              <div id="pThree" className={this.state.pThree}/>
            </div>
          {/*can get rid of this nested grid-div and expand the container-grid at some point would not safe my css changes tho  */}
          {/*link to logIn for now can do that with reactRouter later */}
            <div className="buttons">
            <Link to={'LogIn'} className="logSign" id="logIn">
                <p className="logText">Log In</p>
            </Link>
            <Link to={'signUp'} className="logSign" id="signUp">
                <p className="signText">Sign Up</p>
            </Link>
            </div>
        </div>
        </ReactTouchEvents>
      </div>
    );
  }
}

export default Welcome;
