import React, {Component} from 'react';
import './dropDownStyle.css'

class NumDropDown extends Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render() {
    const numberArr=[0,1,2,3,4,5,6,7,8,9]

    return(
      <select value={this.props.selectedVal} style={{borderRadius: '50%'}} onChange={this.props.setScores} id="numDropDown" name={this.props.name}>{numberArr.map((num,i) => <option key={i} value={num}>{num}</option>)}</select>
    );
  }
}

export default NumDropDown;
