import React, {Component} from 'react';
import './reviewComp.css';


class ConfirmTab extends Component{
  constructor(props){
    super(props);

    this.state = {

    }
  }
  render() {
    const container={
      overflow: "hidden",
      width: "100%",
      height: "100%",
      alignItems: "center"
    };
    const confirmRow={
      width: "100%",
      display: "flex",
      height: "6rem",
      justifyContent: "space-around",
      paddingTop: "15%"

    };
    const deny={
      height: "100%",
      width: "40%",
      borderRadius: "1rem",
      backgroundColor: "#ff9696",
      display: "flex",
      alignItems: "center"
    };
    const confirmStyle={
      height: "100%",
      width: "40%",
      borderRadius: "1rem",
      backgroundColor: "#50e3c2",
      display: "flex",
      alignItems: "center"
    };
    const putMiddle={
      color: "white",
      margin: "0 auto",
      fontSize: "2.5rem"
    };
    const descLine={
      display: "block",
      height: "10%",
      width: "100%",
      fontSize: "0.8rem",
      textAlign: "center",

    };

    return(
      <div style={container}>
        <div style={descLine}><p>YOUR OPPONENT LOGGED THE SCORE EARLIER<br/><br/> DO YOU CONFIRM?</p></div>
        <div style={confirmRow}>
          <div onClick={()=>this.props.confirm(1)} style={confirmStyle}><i style={putMiddle} class="fas fa-check"/></div>
          <div onClick={()=>this.props.confirm(0)} style={deny}><i style={putMiddle} class="fas fa-times"/></div>
        </div>
      </div>
    );
  }
}

export default ConfirmTab;
