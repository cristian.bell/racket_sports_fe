import React, {Component} from 'react';
import './reviewComp.css';
import ReviewLine from './ReviewLine';
import NumDropDown from './NumDropDown.js';
import ConfirmTab from './ConfirmTab.js';
import axios from 'axios';
import Defs from '../Defs.js';

class ConfirmComp extends Component{
  constructor(props){
    super(props);
    this.noShow=this.noShow.bind(this);
    this.setRating=this.setRating.bind(this);
    this.setScores=this.setScores.bind(this);
    this.submitData=this.submitData.bind(this);
    //this.setScoresOpponent=this.setScoresOpponent.bind(this);
    this.confirmRating=this.confirmRating.bind(this);
    this.tempScoreYou=[];
    this.tempScoreOpponent=[];
    this.currentScores=this.props.match.score.split(/,|:/)
    console.log(this.currentScores)
    this.state = {
      reviewArr: [0,0,0,0,0],
      youOne: this.currentScores[0],
      youTwo: this.currentScores[2],
      youThree: this.currentScores[4],
      opOne: this.currentScores[1],
      opTwo: this.currentScores[3],
      opThree: this.currentScores[5],
      noShow: false,
      noShowText: "OPPONENT NO SHOW",
      isBlury: "",
      isDisabled: "isDisabled",
      buttonMode: "FILL IN FIELDS",
      getMyRating: false,
      reviewRating: true,
      sets: 3,
      confirmState: 1
    }
  }
  componentDidMount(){
    
  }
  sendConfirm=(event)=>{
    console.log(this)
    event.preventDefault();
    const confirmRoute=this.state.confirmState? "match/confirm/":"match/dispute/";
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + confirmRoute+this.props.match.id,
      withCredentials: true,
    }).then(response =>{
        console.log(response.data);
        if(response.status === 200){
          this.props.handleSnack("SCORE SUCCESFULLY CONFIRMED",0,false)
        }
    }).catch(response =>{
        //handle error
          
          this.props.handleSnack("SOMETHING WENT WRONG",1,false)
          
        });
        //this.props.logMeIn();
  };
  sendReview=(event)=>{
    event.preventDefault();
    const formData=new FormData();
    formData.append("match_id", this.props.match.id);
    formData.append("manner", this.state.reviewArr[0]);
    formData.append("punctuality", this.state.reviewArr[1]);
    formData.append("power", this.state.reviewArr[2]);
    formData.append("consistency", this.state.reviewArr[3]);
    formData.append("strategy", this.state.reviewArr[4]);
    //formData.append("review_text", "this.state.reviewArr[4]");
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'review/',
      withCredentials: true,
      data: formData,
    }).then(response =>{
        console.log(response.data);

        if(response.status === 200){
          this.props.handleSnack("REVIEW SUCCESFULLY SEND",0,false)
        }
    }).catch(response =>{
        //handle error
          
          this.props.handleSnack("SOMETHING WENT WRONG",1,false)
          
        });
        //this.props.logMeIn();
  };
  setRating(i, j){
    this.myReview=this.state.reviewArr
    this.myReview[j]=i;
    this.setState({
      reviewArr: this.myReview
    })
  }
  setScores(event){
    //disabled for now
    /* const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    },()=>{
      this.checkScore();
    });
 */
  }
  checkScore(){
    console.log(this.state)
    if (this.state.sets === 1 || this.state.sets === '1') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        this.setState({
          validScore: true
        });
        return true;
      }
    }
    if (this.state.sets === 2 || this.state.sets === '2') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        if((this.state.youTwo+this.state.opTwo)>0 && (this.state.youTwo>5 || this.state.opTwo>5)){
          this.setState({
            validScore: true
          });
          return true;
        }
      }
    }
    if (this.state.sets === 3 || this.state.sets === '3') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        if((this.state.youTwo+this.state.opTwo)>0 && (this.state.youTwo>5 || this.state.opTwo>5)){
          if((this.state.youThree+this.state.opThree)>0 && (this.state.youThree>5 || this.state.opThree>5)){
            this.setState({
              validScore: true
            });
            return true;
          }
        }
      }
    }
  }
  confirmRating(confirm){
    this.setState({
      getMyRating: true,
      reviewRating: false,
      confirmState: confirm
    });
  }
  submitData(event){
    event.preventDefault();
    this.sendConfirm(event);
    this.sendReview(event);
    this.props.submitReview(event);

  }
  noShow(event){
    event.preventDefault();
    if(!this.state.noShow){
    this.setState({
      noShowText: "UNDO",
      noShow: true,
      reviewArr: [0,0,0,0,0],
      isBlury: "blury"
    });
    return 1;
  }if(this.state.noShow){
    this.setState({
      noShowText: "OPONNENT NO SHOW",
      noShow: false,
      reviewArr: [0,0,0,0,0],
      isBlury: "nonBlury"
    })
  }
  }
  render() {
    const reviewComp={
      zIndex: "5",
      position: "absolute",
      top: "0",
      left: "0",
      height: "100%",
      width: "100%",

      backgroundColor: "rgba(255,255,255,0.9)",

    };
    return(
      <div style={reviewComp}>
        <div className="closeButton" onClick={this.props.close}><i class="fas fa-times"/></div>
      <div id="reviewCont">
        <div id="gameOutcome">
          <div className="rateHeaders">ENTER THE SCORES</div>
          <div className='newScore'><div className='introTxt'>YOU</div>
            <div className='inputContainNum'>
            <NumDropDown selectedVal={this.state.youOne} name='youOne' setScores={this.setScores}/>
            {this.state.sets>1 && <NumDropDown selectedVal={this.state.youTwo} name='youTwo' setScores={this.setScores}/>}
            {this.state.sets>2 && <NumDropDown selectedVal={this.state.youThree} name='youThree' setScores={this.setScores}/>}
            </div>
        </div>
          <div className='newScore'><div className='introTxt'>{this.props.match.o_name}</div>
            <div className='inputContainNum'>
            <NumDropDown selectedVal={this.state.opOne} name='opOne' setScores={this.setScores}/>
            {this.state.sets>1 && <NumDropDown selectedVal={this.state.opTwo} name='opTwo' setScores={this.setScores}/>}
            {this.state.sets>2 && <NumDropDown selectedVal={this.state.opThree} name='opThree' setScores={this.setScores}/>}
            </div>
          </div>
          <div id="midButtonPos"><button onClick={this.noShow} className={"greyButton textCentered"}><p>{this.state.noShowText}</p></button></div>
        </div>


         {this.state.reviewRating && <div className="gameRatings"><ConfirmTab confirm={this.confirmRating}/></div>}
         {this.state.getMyRating&&<div className="gameRatings">
         <div className={"rateHeaders "+this.state.isBlury}>RATE YOUR OPPONENT</div>
         <div id="rateCont" className={this.state.isBlury}>
           <div id="rateLabel">MANNER</div>
           <div id="rateStars"><ReviewLine pos={0} setStar={this.setRating} review={this.state.reviewArr[0]}/></div>
         </div>
         <div id="rateCont" className={this.state.isBlury}>
           <div id="rateLabel">PUNCTUALITY</div>
           <div id="rateStars"><ReviewLine pos={1} setStar={this.setRating} review={this.state.reviewArr[1]}/></div>
         </div>
         <div id="rateCont" className={this.state.isBlury}>
           <div id="rateLabel">POWER</div>
           <div id="rateStars"><ReviewLine pos={2} setStar={this.setRating} review={this.state.reviewArr[2]}/></div>
         </div>
         <div id="rateCont" className={this.state.isBlury}>
           <div id="rateLabel">CONSISTENCIES</div>
           <div id="rateStars"><ReviewLine pos={3} setStar={this.setRating} review={this.state.reviewArr[3]}/></div>
         </div>
         <div id="rateCont" className={this.state.isBlury}>
           <div id="rateLabel">STRATEGY</div>
           <div id="rateStars"><ReviewLine pos={4} setStar={this.setRating} review={this.state.reviewArr[4]}/></div>
         </div>
         <div id="buttonDivBottom"><button onClick={this.submitData} className={"black-rounded textCentered"}><p>SUBMIT</p></button></div>

         </div>}

      </div>
      </div>
    );
  }
}

export default ConfirmComp;
