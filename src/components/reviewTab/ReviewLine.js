import React, {Component} from 'react';
import './reviewComp.css';


class ReviewLine extends Component{
  constructor(props){
    super(props);

    this.state = {

    }
  }
  componentDidMount(){
    console.log("Review Line component did mount");
  }

  render() {

    const rateStars={

      width: "100%",
      margin: "0 auto",
      display: "flex",
      justifyContent: "space-around",
      alignItems: "center"
    }
    const reviewStar={
      height: "1.4rem",
      width: "1.4rem",
      display: "flex",
      alignItems: "center"
    }
    const star={
      margin: "0 auto",
      fontSize: "1.2rem"
    }

    return(

        <div style={rateStars}>
          <div onClick={()=>this.props.setStar(1, this.props.pos)} style={reviewStar}>{this.props.review>0 && <i style={star} className="fas fa-star"/>}{this.props.review<=0 && <i style={star} className="far fa-star"></i>}</div>
          <div onClick={()=>this.props.setStar(2, this.props.pos)} style={reviewStar}>{this.props.review>1 && <i style={star} className="fas fa-star"/>}{this.props.review<=1 && <i style={star} className="far fa-star"></i>}</div>
          <div onClick={()=>this.props.setStar(3, this.props.pos)} style={reviewStar}>{this.props.review>2 && <i style={star} className="fas fa-star"/>}{this.props.review<=2 && <i style={star} className="far fa-star"></i>}</div>
          <div onClick={()=>this.props.setStar(4, this.props.pos)} style={reviewStar}>{this.props.review>3 && <i style={star} className="fas fa-star"/>}{this.props.review<=3 && <i style={star} className="far fa-star"></i>}</div>
          <div onClick={()=>this.props.setStar(5, this.props.pos)} style={reviewStar}>{this.props.review>4 && <i style={star} className="fas fa-star"/>}{this.props.review<=4 && <i style={star} className="far fa-star"></i>}</div>
        </div>

    );
  }
}

export default ReviewLine;
