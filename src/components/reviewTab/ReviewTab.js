import React, {Component} from 'react';
import './reviewComp.css';
import ReviewLine from './ReviewLine';
import NumDropDown from './NumDropDown.js';
import axios from 'axios';
import Defs from '../Defs.js';

class ReviewComp extends Component{
  constructor(props){
    super(props);
    console.log(this.props)
    this.noShow=this.noShow.bind(this);
    this.setRating=this.setRating.bind(this);
    this.setScores=this.setScores.bind(this);
    this.submitData=this.submitData.bind(this);
    this.checkScore=this.checkScore.bind(this);
    //this.setScoresOpponent=this.setScoresOpponent.bind(this);
    this.tempScoreYou=[];
    this.tempScoreOpponent=[];
    this.testForZero=0;
    this.isDisabled='';
    this.buttonMode='SUBMIT';
    this.state = {
      validScore: false,
      reviewArr: [0,0,0,0,0],
      sets: 3,
      yourScore: [],
      opponentScore: [],
      youOne: 0,
      youTwo: 0,
      youThree: 0,
      opOne: 0,
      opTwo: 0,
      opThree: 0,
      noShow: false,
      noShowText: "OPPONENT NO SHOW",
      isBlury: "",
      isDisabled: "isDisabled",
      buttonMode: "FILL IN FIELDS"
    }
  }
  componentDidMount(){

  }
  sendReview=(event)=>{
    event.preventDefault();
    const formData=new FormData();
    formData.append("match_id", this.props.match.id);
    formData.append("manner", this.state.reviewArr[0]);
    formData.append("punctuality", this.state.reviewArr[1]);
    formData.append("power", this.state.reviewArr[2]);
    formData.append("consistency", this.state.reviewArr[3]);
    formData.append("strategy", this.state.reviewArr[4]);
    formData.append("score", 
    this.state.youOne+":"+this.state.opOne+","
    +this.state.youTwo+":"+this.state.opTwo+","
    +this.state.youThree+":"+this.state.opThree);
    //formData.append("review_text", "this.state.reviewArr[4]");
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'review/',
      withCredentials: true,
      data: formData,
    }).then(response =>{
        console.log(response.data);

        if(response.status === 200){
          this.props.handleSnack("REVIEW SUCCESFULLY SEND",0,false)
        }
    }).catch(response =>{
        //handle error
          
          this.props.handleSnack("SOMETHING WENT WRONG",1,false)
          
        });
        //this.props.logMeIn();
  };
  /* sendScores=(event)=>{
    event.preventDefault();
    if(this.state.validScore){
      const formData=new FormData();
    formData.append("score", 
    this.state.youOne+" : "+this.state.opOne+" , "
    +this.state.youTwo+" : "+this.state.opTwo+" , "
    +this.state.youThree+" : "+this.state.opThree);
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'scores/',
      withCredentials: true,
      data: formData,
    }).then(response =>{     
        console.log(response)
        if(response.status === 200){
       
        }
    }).catch(response =>{
        //handle error
        console.log(response)
        });

    }
    
  } */
  setRating(i, j){
    this.myReview=this.state.reviewArr
    this.myReview[j]=i;
    this.setState({
      reviewArr: this.myReview
    })
  }

  setScores(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    },()=>{
      this.checkScore();
    });

  }
/*   setScoresOpponent(event){
    event.preventDefault();
    this.tempScoreOpponent.push(event.target.value);
    this.setState({
      opponentScore: this.tempScoreOpponent,
      isDisabled: "",
      buttonMode: "SUBMIT"
    })
  } */
  submitData(event){
    event.preventDefault();
    if (true) {
      this.sendReview(event);
      //this.sendScores(event);
      this.props.submitReview(event,this.state);
      this.props.close();
      this.setState({
        isDisabled: "",
        buttonMode: "SUBMIT"
      })
    }
  }
  noShow(event){
    event.preventDefault();
    if(!this.state.noShow){
    this.setState({
      noShowText: "UNDO",
      noShow: true,
      reviewArr: [0,0,0,0,0],
      isBlury: "blury"
    })
    return 1;
  }if(this.state.noShow){
    this.setState({
      noShowText: "OPONNENT NO SHOW",
      noShow: false,
      reviewArr: [0,0,0,0,0],
      isBlury: "nonBlury"
    })
  }
  }
  checkScore(){
    console.log(this.state)
    if (this.state.sets === 1 || this.state.sets === '1') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        this.setState({
          validScore: true
        });
        return true;
      }
    }
    if (this.state.sets === 2 || this.state.sets === '2') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        if((this.state.youTwo+this.state.opTwo)>0 && (this.state.youTwo>5 || this.state.opTwo>5)){
          this.setState({
            validScore: true
          });
          return true;
        }
      }
    }
    if (this.state.sets === 3 || this.state.sets === '3') {
      if((this.state.youOne+this.state.opOne)>0 && (this.state.youOne>5 || this.state.opOne>5)){
        if((this.state.youTwo+this.state.opTwo)>0 && (this.state.youTwo>5 || this.state.opTwo>5)){
          if((this.state.youThree+this.state.opThree)>0 && (this.state.youThree>5 || this.state.opThree>5)){
            this.setState({
              validScore: true
            });
            return true;
          }
        }
      }
    }
  }
  render() {
    //const numberArr=[1,2,3,4,5,6,7,8,9];
    const reviewComp={
      zIndex: "5",
      position: "absolute",
      top: "0",
      left: "0",
      height: "100%",
      width: "100%",

      backgroundColor: "rgba(255,255,255,1)",
      backgroundImage: 'linear-gradient(#fffefa 40%,rgba(212,255,67,1))',
      backgroundAttachment: 'fixed',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover'
    }
    //const myReviews=[5,4,2,3,1];
    return(
      <div style={reviewComp}>
        <div className="closeButton" onClick={this.props.close}><i class="fas fa-times"/></div>
      <div id="reviewCont">
        <div id="gameOutcome">
          <div className="rateHeaders">ENTER THE SCORES</div>
          <div className='newScore'><div className='introTxt'>YOU</div>
            <div className='inputContainNum'>
            <NumDropDown name='youOne' setScores={this.setScores}/>
            {this.state.sets>1 && <NumDropDown name='youTwo' setScores={this.setScores}/>}
            {this.state.sets>2 && <NumDropDown name='youThree' setScores={this.setScores}/>}
            </div>
        </div>
          <div className='newScore'><div className='introTxt'>{this.props.match.o_name}</div>
            <div className='inputContainNum'>
            <NumDropDown name='opOne' setScores={this.setScores}/>
            {this.state.sets>1 && <NumDropDown name='opTwo' setScores={this.setScores}/>}
            {this.state.sets>2 && <NumDropDown name='opThree' setScores={this.setScores}/>}
            </div>
          </div>
          <div id="midButtonPos"><button onClick={this.noShow} className={"greyButton textCentered"}><p>{this.state.noShowText}</p></button></div>
        </div>
        {this.state.noShow&&<div id="noShowOverlay">
          <p>RATING FUNCTION DISABLED</p>
        </div>}
        <div className="gameRatings">

          <div className={"rateHeaders "+this.state.isBlury}>RATE YOUR OPPONENT</div>
          <div id="rateCont" className={this.state.isBlury}>
            <div id="rateLabel">MANNER</div>
            <div id="rateStars"><ReviewLine pos={0} setStar={this.setRating} review={this.state.reviewArr[0]}/></div>
          </div>
          <div id="rateCont" className={this.state.isBlury}>
            <div id="rateLabel">PUNCTUALITY</div>
            <div id="rateStars"><ReviewLine pos={1} setStar={this.setRating} review={this.state.reviewArr[1]}/></div>
          </div>
          <div id="rateCont" className={this.state.isBlury}>
            <div id="rateLabel">POWER</div>
            <div id="rateStars"><ReviewLine pos={2} setStar={this.setRating} review={this.state.reviewArr[2]}/></div>
          </div>
          <div id="rateCont" className={this.state.isBlury}>
            <div id="rateLabel">CONSISTENCIES</div>
            <div id="rateStars"><ReviewLine pos={3} setStar={this.setRating} review={this.state.reviewArr[3]}/></div>
          </div>
          <div id="rateCont" className={this.state.isBlury}>
            <div id="rateLabel">STRATEGY</div>
            <div id="rateStars"><ReviewLine pos={4} setStar={this.setRating} review={this.state.reviewArr[4]}/></div>
          </div>
          <div id="buttonDivBottom"><button onClick={this.submitData} id={this.isDisabled} className={"black-rounded textCentered"}><p>{this.buttonMode}</p></button></div>
        </div>

      </div>
      </div>
    );
  }
}

export default ReviewComp;
