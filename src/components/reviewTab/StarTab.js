import React, {Component} from 'react';
import './reviewComp.css';


class ReviewLine extends Component{
  constructor(props){
    super(props);

    this.state = {

    }
  }


  render() {


    return(
        <div>
          <div id="rateCont">
            <div id="rateLabel">MANNER</div>
            <div id="rateStars"><ReviewLine pos={0} setStar={this.setRating} review={this.state.reviewArr[0]}/></div>
          </div>
          <div id="rateCont">
            <div id="rateLabel">PUNCTUALITY</div>
            <div id="rateStars"><ReviewLine pos={1} setStar={this.setRating} review={this.state.reviewArr[1]}/></div>
          </div>
          <div id="rateCont">
            <div id="rateLabel">POWER</div>
            <div id="rateStars"><ReviewLine pos={2} setStar={this.setRating} review={this.state.reviewArr[2]}/></div>
          </div>
          <div id="rateCont">
            <div id="rateLabel">CONSISTENCIES</div>
            <div id="rateStars"><ReviewLine pos={3} setStar={this.setRating} review={this.state.reviewArr[3]}/></div>
          </div>
          <div id="rateCont">
            <div id="rateLabel">STRATEGY</div>
            <div id="rateStars"><ReviewLine pos={4} setStar={this.setRating} review={this.state.reviewArr[4]}/></div>
          </div>
        </div>
    );
  }
}

export default StarTab;
