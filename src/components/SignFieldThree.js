import React, {Component} from 'react';
import './SignUp.css';
import './SignForm.css';
import DefFunctions from "./DefFunctions";
import {Redirect} from "react-router-dom";
import Defs from "./Defs";

class SignFieldThree extends Component{
    constructor(props){
    super(props);
    this.state = {
        signUpFinished : false,
        backToStep1: false,
        backToStep2: false,
        backToStep3: false,
    }
}
finishStage(event){
    event.preventDefault();
    let defFunctions = new DefFunctions();
    if (defFunctions.checkFormDataStep1(this.props, this) !== true) {
        this.setState({
            signUpFinished: false,
            backToStep1: true,
            fieldError: true,
        });
        return false;
    }

    if (defFunctions.checkFormDataStep2(this.props, this) !== true) {
        this.setState({
            backToStep2: true,
            signUpFinished: false,
            fieldError: true,
        });
        return false;
    }

    if (defFunctions.checkFormDataStep3(this.props, this) !== true) {
        this.setState({
            signUpFinished: false,
            fieldError: true,
        });
        return false;
    }

    this.props.handleSubmit(event);
    //console.log(this.props);
    this.setState({
        signUpFinished: true
    });
}

render() {
    if(this.state.backToStep1) return(<Redirect to={{
        pathname: '/signUp',
    }} />);
    if(this.state.backToStep2) return(<Redirect to={{
        pathname: '/signUp/signFieldTwo',
    }} />);
    return (
      <div className="signUpContainTr">
          {this.state.fieldError&&<div className="passwordError">{Defs.formErrorMessage}</div>}
        <form  onSubmit={this.finishStage.bind(this)} className="signUpFormTwo">
          <p className="inputsP" id="handP">DOMINANT HAND</p>
          <div className="radios">
            <input value="2" onChange={this.props.handleInputChange} checked={this.props.dominantHand === "2"}  type="radio" name="dominantHand" className="handRadio" id="rightRadio"/>
            <label id="right" className="handLabel" htmlFor="rightRadio"><p>LEFT</p></label>
            <input value="1" onChange={this.props.handleInputChange} checked={this.props.dominantHand === "1"} type="radio" name="dominantHand" className="handRadio" id="leftRadio"/>
            <label id="left" className="handLabel" htmlFor="leftRadio"><p>RIGHT</p></label>
          </div>
          <p className="inputsP" id="backHandP">BACKHAND STYLE</p>
          <div className="radios">
            <input value="1" onChange={this.props.handleInputChange} checked={this.props.backHand === "1"}  type="radio" name="backHand" className="handRadio" id="rightOffRadio"/>
            <label id="male" className="handLabel" htmlFor="rightOffRadio"><p>SINGLE</p></label>
            <input value="2" onChange={this.props.handleInputChange} checked={this.props.backHand === "2"} type="radio" name="backHand" className="handRadio" id="leftOffRadio"/>
            <label id="female" className="handLabel" htmlFor="leftOffRadio"><p>DOUBLE</p></label>
          </div>
          <div id="playerFrequDiv">
          <p className="inputsP" id="freqP">TIMES YOU PLAY PER MONTH</p>
          <div className="radios" id="frequRadios">
            <input value="1" onChange={this.props.handleInputChange} checked={this.props.playFrequence === "1"}  type="radio" name="playFrequence" className="playFrequRadio" id="rarRadio"/>
            <label id="rar" className="playFrequenceLabel" htmlFor="rarRadio"><p>0-5</p></label>
            <input value="2" onChange={this.props.handleInputChange} checked={this.props.playFrequence === "2"} type="radio" name="playFrequence" className="playFrequRadio" id="notOftenRadio"/>
            <label id="notOften" className="playFrequenceLabel" htmlFor="notOftenRadio"><p>5-10</p></label>
              <input value="3" onChange={this.props.handleInputChange}  checked={this.props.playFrequence === "3"}  type="radio" name="playFrequence" className="playFrequRadio" id="OftenRadio"/>
              <label id="often" className="playFrequenceLabel" htmlFor="OftenRadio"><p>10-20</p></label>
              <input value="4" onChange={this.props.handleInputChange}  checked={this.props.playFrequence === "4"} type="radio" name="playFrequence" className="playFrequRadio" id="superOftenRadio"/>
              <label id="superOften" className="playFrequenceLabel" htmlFor="superOftenRadio"><p>20+</p></label>
          </div>

          </div>
          <p className="inputsP" id="experienceP">YEARS OF PLAYING</p>
          <div id="experienceDiv">
            <div className="radios" id="experienceRadios">
              <input value="1" onChange={this.props.handleInputChange} checked={this.props.playExp === "1"}  type="radio" name="playExp" className="playFrequRadio" id="exp1Radio"/>
              <label id="exp1" className="playFrequenceLabel" htmlFor="exp1Radio"><p>0-5</p></label>
              <input value="2" onChange={this.props.handleInputChange} checked={this.props.playExp === "2"} type="radio" name="playExp" className="playFrequRadio" id="exp2Radio"/>
              <label id="exp2" className="playFrequenceLabel" htmlFor="exp2Radio"><p>5-10</p></label>
                <input value="3" onChange={this.props.handleInputChange}  checked={this.props.playExp === "3"}  type="radio" name="playExp" className="playFrequRadio" id="exp3Radio"/>
                <label id="exp3" className="playFrequenceLabel" htmlFor="exp3Radio"><p>10-20</p></label>
                <input value="4" onChange={this.props.handleInputChange}  checked={this.props.playExp === "4"} type="radio" name="playExp" className="playFrequRadio" id="exp4Radio"/>
                <label id="exp3" className="playFrequenceLabel" htmlFor="exp4Radio"><p>20+</p></label>
            </div>
          </div>
          <p className="inputsP" id="styleP">PLAY STYLE (choose 2)</p>
          <div id="styleDiv">
           <div id="styleOne">
             <input value="1" onChange={this.props.handleInputChange} checked={this.props.mainSkill === "1"} name="mainSkill" id="styleCheck1" type="radio"/>
             <label id="sForehand" name="playStyle" className="checkLabel" htmlFor="styleCheck1"/>
             <p>Strong Forehand</p>
             <input value="2" onChange={this.props.handleInputChange} checked={this.props.mainSkill === "2"} name="mainSkill" id="styleCheck2" type="radio"/>
             <label id="sBackhand" name="playStyle" className="checkLabel" htmlFor="styleCheck2"/>
              <p>Strong Backhand</p>
             <input value="3" onChange={this.props.handleInputChange} checked={this.props.mainSkill === "3"} name="mainSkill" id="styleCheck3" type="radio"/>
             <label id="sBaseliner" name="playStyle" className="checkLabel" htmlFor="styleCheck3"/>
             <p>Baseliner</p>
           </div>

            <div id="styleTwo">
             <input value="4" onChange={this.props.handleInputChange} checked={this.props.mainSkill2 === "4"} name="mainSkill2" id="styleCheck4" type="radio"/>
             <label id="sServer" name="playStyle" className="checkLabel" htmlFor="styleCheck4"/>
             <p>Serve & Volley</p>
             <input value="5" onChange={this.props.handleInputChange} checked={this.props.mainSkill2 === "5"} name="mainSkill2" id="styleCheck5" type="radio"/>
             <label id="sTactical" name="playStyle" className="checkLabel" htmlFor="styleCheck5"/>
             <p>Tactical</p>
             <input value="6" onChange={this.props.handleInputChange} checked={this.props.mainSkill2 === "6"} name="mainSkill2" id="styleCheck6" type="radio"/>
            <label id="sAllround" name="playStyle" className="checkLabel" htmlFor="styleCheck6"/>
             <p>All Round</p>
           </div>
          </div>

        <input type="submit" value="CREATE ACCOUNT" className="signInput" id="signSubmitBig"/>
      </form>

  </div>

    );
  }
}

export default SignFieldThree;
