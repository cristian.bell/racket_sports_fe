import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import './LogInFin.css';


class SignUpFinished extends Component{
  constructor(props){
    super(props);

    this.state = {
        //faking login
        forwardToAcc: true,
    };
  }

  getAccount(){
    this.setState({
      fowardToAcc: true
    })
  }

  render() {

    return (

      <div className="containerLog">
      {this.state.forwardToAcc && (<Redirect to={{
                pathname: '/home',
                state: {
                    reference: this.state.userName}
            }} />)}
        <div><h1>QUIET PLEASE.</h1></div>
        <div onClick={this.getAccount.bind(this)} id="readyPos"><div id="readyIcon"/>
        <p>YOU ARE READY!</p>
        </div>

      <div id="noAccSignUp" className="fieldStripes">
          <div id="horizontalStripe"/>
          <div id="verticalStripe"/>
          <div id="finishedButtonPos"><button onClick={this.getAccount.bind(this)} className="signInput" id="logSubmit">START NOW</button></div>
        </div>

      </div>
    );
  }
}

export default SignUpFinished;
