import React, {Component} from 'react';
import { Link} from 'react-router-dom';

class ProgressBar extends Component{
  constructor(props){
    super(props);
    if(parseInt(this.props.stage, 10) === 1){
      this.ringOne="ring";
    } else if(parseInt(this.props.stage, 10) === 2) {
      this.ringTwo="ring";
    } else if(parseInt(this.props.stage, 10) === 3) {
      this.ringThree="ring";
    }

    this.state = {
       ringState: this.props.stage
      }
  }



render(){
return(

  <div className="progress">
    <Link className="progressLink" to = {'/signUp'}>
      <div className={this.ringOne} id="pOn"><span>1</span></div>
    </Link>
    <Link className="progressLink" to = {'/signUp/SignFieldTwo'}>
      <div className={this.ringTwo} id="pTwo"><span>2</span></div>
    </Link>
    <Link className="progressLink" to = {'/signUp/signFieldThree'}>
      <div className={this.ringThree} id="pThree"><span>3</span></div>
    </Link>
  </div>

);
}
}

export default ProgressBar;
