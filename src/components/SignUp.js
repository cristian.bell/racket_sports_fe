import React, {Component} from 'react';
import {Redirect, Switch, Route, Link} from 'react-router-dom';
import axios from 'axios';
import SignField from './SignField.js';
import SignFieldTwo from './SignFieldTwo.js';
import SignFieldThree from './SignFieldThree.js';
import UstInfo from './UstInfo.js';
import ProgressBar from './Register/ProgressBar.js'
import Defs from './Defs.js';
import DefFunctions from './DefFunctions.js';

import './SignUp.css';


class SignUp extends Component{
    constructor(props){
        super(props);
        this.toggleDesc = this.toggleDesc.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.updateExperience = this.updateExperience.bind(this);
        this.changeExperience = this.changeExperience.bind(this);
        this.changeExperiencePlus = this.changeExperiencePlus.bind(this);
        this.changeExperienceMinus = this.changeExperienceMinus.bind(this);
        this.state = {
            formError: '',
            formErrorMessage: '',
            overlay: false,
            logInStage: 0,
            backgroundHide: "",
            passwordMatch: false,
            emailValid: false,

            userName: "",
            firstName: "",
            lastName: "",
            password: "",
            passwordRe: "",
            email: "",

            gender: null,
            genderChoice: Defs.genders,
            city: 0,
            cityChoice: Defs.cities,
            ageGroup: null,
            lvlSlider : 40,
            country: "Germany",

            dominantHand: "right",
            backHand: "single",
            playFrequence: null,
            experience: null,
            playExp: null,
            experienceOptions: ["0-5","5-10","10-20","20+"],
            mainSkill: null,
            mainSkill2: null,
            goToProfile: false
        }
    }


    toggleDesc(event){
        event.preventDefault();
        this.setState(prevState=>({
            overlay: !prevState.overlay,

        }));
        if(this.state.backgroundHide === "hidden"){
            this.setState({
                backgroundHide: ""
            });
        }else{
            this.setState({
                backgroundHide: "hidden"
            });
        }
    }

    updateExperience(event){
        this.setState({
            experience : parseInt(event.target.value, 10)
        });

    }
    changeExperience(i){

        if (
            (i === -1 && this.state.experience === 0) ||
            (i === 1 && this.state.experience === this.state.experienceOptions.length-1)) {
            return 1;
        }else{
            this.setState({
                experience : i + this.state.experience
            });
        }

        return 1;
    }
    changeExperiencePlus(){
        this.changeExperience(1);
    }
    changeExperienceMinus(){
        this.changeExperience(-1);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        /*console.debug('ev tgval: ' + event.target.value);
        console.debug('statename: ' + this.state[name]);*/

        this.setState({
            [name]: value
        });

    }
    handleSubmit(event){
        event.preventDefault();

        //check Form data
        const formData=new FormData();
        formData.append("email", this.state.email);
        formData.append("pass", this.state.password);
        formData.append("firstname", this.state.firstName);
        formData.append("lastname", this.state.lastName);

        formData.append("gender",this.state.gender);
        formData.append("age",this.state.ageGroup);
        formData.append("city", this.state.city);
        formData.append("level",this.state.lvlSlider);

        formData.append("dominant_hand", this.state.dominantHand);
        formData.append("backhand_style",this.state.backHand);
        formData.append("play_times",this.state.playFrequence);
        formData.append("play_years",this.state.playExp);
        formData.append("main_skill",this.state.mainSkill);
        formData.append("main_skill2",this.state.mainSkill2);

        axios({ method: 'post', url: Defs.be_url + 'user/save'/*,headers:{'x-api-key': '123'}*/,  data: formData})
            .then(response =>{
                if(response.status === 200){
                    this.setState({
                        goToProfile: true
                    })
                }
            })
            .catch(response =>{
                console.log(response);
            });
        this.setState({
            goToProfile: true
        });
    }

    containerClass="hidden";
    render() {
        return (
            <div style={{height: window.innerHeight+"px"}} >
                {this.state.goToProfile && (<Redirect to={'../SignUpFinished'}/>)}
                <div id="containerSign" className={this.state.backgroundHide}>
                    <h1 id="quietPls">QUIET PLEASE.{this.state.formErrorMessage}</h1>
                    {/*TODO component */}
                    <Route exact path='/signUp' render={()=><ProgressBar stage="1"/>}/>
                    <Route path='/signUp/signFieldTwo' render={()=><ProgressBar stage="2"/>}/>
                    <Route path='/signUp/signFieldThree' render={()=><ProgressBar stage="3"/>}/>
                    {/*TODO create component with message and dots as state */}

                    <Switch>
                        <Route exact path='/signUp' render={()=><SignField {...this.state} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} stage="1" />}/>
                        <Route path='/signUp/signFieldTwo' render={()=><SignFieldTwo {...this.state} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit} stage="2" toggleIt={this.toggleDesc}/>}/>
                        <Route path='/signUp/signFieldThree' render={()=><SignFieldThree {...this.state} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit}/>}/>
                    </Switch>

                    <div id="haveAcc">
                        <p id="gotAcc">ALREADY HAVE AN ACCOUNT?</p>
                        <Link to={'../LogIn'} id="logInLink">LOGIN </Link>
                    </div>
                </div>
                {this.state.overlay && <UstInfo toggleIt={this.toggleDesc} />}
                {this.state.overlay && <div id="close" onClick={this.toggleDesc}><i className="fas fa-times" /></div>}

            </div>
        );
    }
}

export default SignUp;
