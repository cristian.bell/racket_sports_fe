import {Component} from "react";
import React from "react";

class Defs extends Component {
    static be_url = 'https://bend.cristianbell.com/';
    static be_url = 'https://bcapp.cristianbell.com/'; //testing
    static be_url = 'http://tpointbe.ubuntuws:8080/'; //dev


    static pwd_min_len = 6;
    static email_min_len = 6;
    static firstname_min_len = 2;

    static cities = ["Berlin", "Hong Kong", "Macau"];
    static genders = ['male', 'female'];
    static tournamentGender = ["man", "woman", "divers"];
    static dominantHand = ['right', 'left'];
    static backhandStyle = ['single', 'double'];

    static elim_rules_val = ["KNOCK OUT", "ROUND ROBIN"];
    static max_players = 16;

    static formErrorMessage = '';
    static formError = false;

    constructor(props){
        super();
        this.state = {
        }
    }
}

export default Defs;