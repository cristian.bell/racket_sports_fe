import React, { Component } from 'react'
import './SnackBar.css'

export default class componentName extends Component {
constructor(props) {
  super(props)
  this.colorChoice=["#323232","red"]
  this.fontColor=["white","black"]
  this.state = {
     message: 'SUCCESFULLY CREATE YOUR TOURNAMENT',
     buttonCont: this.props.buttonContent || "GO!"
  }
}
  
  render() {
    let snackMessageStyle={
        maxWidth:this.props.withConfirm? "70%" : "100%",
    }
    let snackBarStyle={
        backgroundColor: this.colorChoice[this.props.state],
        color: this.fontColor[this.props.state]
    }
    return (
      <div onClick={this.props.onClickHandler} style={snackBarStyle} id='snackBarCont'>
        <div id='snackMessage' style={snackMessageStyle}>{this.props.message}</div>
        {this.props.withConfirm&&<div /* onClick={()=>console.log('snackbar WORKED')} */ onClick={this.props.actionHandler} id='snackAction'>{this.state.buttonCont}</div>}
      </div>
    )
  }
}
