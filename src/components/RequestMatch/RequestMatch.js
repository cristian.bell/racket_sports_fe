import React, {Component} from 'react';
import './requestMatch.css'
import ProfileImage from '../profile/ProfileImage.js';



class RequestMatch extends Component{
  constructor(props){
    super(props);
      this.state = {
        player: {},
        requestedPlayer: {},

    }
  }
  componentDidMount(){

  }


  render() {
    return (
      <div className="requestContainer">
        <div onClick={this.props.closeWindow}  id="closeWindow"><i id="myCloseIcon" class="fas fa-times"></i></div>
        <div id="requestContent">
          <h1>ARRANGE A MATCH</h1>
        <div id="imageField">
          <div id="userImage"><ProfileImage source={this.props.player.profilePic}/>
            <p id="requestNames">{this.props.userData.name}</p>
            <p id="requestLevel">{this.props.userData.playerSkill}</p>
          </div>
          <div id="inBetween"><p>VERSUS</p></div>
          <div id="requestImage"><ProfileImage source={this.props.userData.profilePic}/>
            <p id="requestNames">{this.props.player.name}</p>
            <p id="requestLevel">{this.props.userData.playerSkill}</p>
          </div>
        </div>
        </div>
      </div>
    );
  }
}

export default RequestMatch;
