import React, {Component} from 'react';
import { Redirect, Link} from 'react-router-dom';
import axios from 'axios';
import './LogIn.css';
import ShowPwd from './helper/ShowPwd.js'
import Defs from './Defs.js';

class LogIn extends Component{
  constructor(props){
    super(props);

    this.handleInputChange=this.handleInputChange.bind(this);
    this.logMeIn=this.logMeIn.bind(this);

    this.state = {
      password: "",
      userName: "",
      userData: {},
      showError: false,
      logMessage: "",

      showPassword: false
    }
  }
  logMeIn(event){
    event.preventDefault();

    if (typeof this.state.userName === 'string' && this.state.userName.length > 5
        && typeof this.state.password === 'string' && this.state.password.length > 1) {

        const formData=new FormData();
        formData.append("email", this.state.userName);
        formData.append("pass", this.state.password);

        axios({
            method: 'post',
            //headers:{'x-api-key': '123'},
            url: Defs.be_url + 'user/login',
            withCredentials: true,
            data: formData,
        }).then(response =>{
            console.log(response.data);
            let userData=response.data;
            this.setState({userData});

            if(response.status === 200){
                sessionStorage.setItem('MPuId',(userData.id))
                this.props.logMeIn();
            }
        }).catch(response =>{
            //handle error
            console.log(response);
            this.setState({
                logMessage: "WRONG USERNAME OR PASSWORD",
                showError: true
            })
        });
        //this.props.logMeIn();
    }
  }
   /* logMeIn(event) {
        event.preventDefault();
        const data=new FormData();
        data.append("email", this.state.userName);
        data.append("pass", this.state.password);


        let xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                console.log(this.responseText);
                //this.props.logMeIn();

            }
        });

        xhr.open("POST", "http://tpointbe.ubuntuws/user/login");
        xhr.setRequestHeader("x-api-key", "123");
        xhr.setRequestHeader("cache-control", "no-cache");
        //xhr.setRequestHeader("Postman-Token", "e9c8fe90-e69a-4091-b53a-481811a0a9e7");

        xhr.send(data);
    }*/

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

  }

  render() {

    if(this.props.logInState)return(<Redirect to={{
              pathname: '/home'
          }} />);

    return (
      <div style={{minHeight: window.innerHeight+"px"}}  className="fullPageContainer">
        <h1>QUIET PLEASE.</h1>
        <div className={"roundedFormContainer maxWidthHeight"}>
          {this.state.showError&&<div className="errorMessage">{this.state.logMessage}</div>}
        <form onSubmit={this.logMeIn} className="logInForm">
          <input onChange={this.handleInputChange} placeholder="Username" type="text" name="userName" className="roundedInput" id="userNameInput"/>
          
          <ShowPwd handleInputChange={this.handleInputChange}></ShowPwd>
          <input type="submit" value="LOGIN" className="roundedSubmitBlack" />
        </form>
      </div>
      <div id="logNoAccSignUp">
        <div id="logHorizontalStripe">
            <Link className="undecoratedLink" to="./SignUp">
                <p id="horizontalStripeP">DON´T HAVE AN ACCOUNT?<br/>SIGN UP HERE.</p>
            </Link>
        </div>
        <div id="logVerticalStripe"/>
      </div>

      </div>
    );
  }
}

export default LogIn;
