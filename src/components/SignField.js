import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';
import DefFunctions from './DefFunctions.js';
import Defs from './Defs.js';
import './SignUp.css';
import ShowPwd from './helper/ShowPwd.js'


class SignField extends Component{
    constructor(props){
        super(props);
        this.state = {
            signUpFinished1 : false,
            fieldError: false,
            fieldMessage1: '',
        }
    }
    handleSubmit(event){
        event.preventDefault();
        this.setState({
            signUpFinished1 : true,
        })
    }
    finishStage(event){
        event.preventDefault();

        let defFunctions = new DefFunctions();
        return defFunctions.checkFormDataStep1(this.props, this);
    }

    render() {
        return (
            <div className="signUpContain">
                {(Defs.formError) && <div className="passwordError">{Defs.formErrorMessage}</div>}
                {this.state.signUpFinished1 && (<Redirect to={'/signUp/SignFieldTwo'}/>)}
                <form onSubmit={this.finishStage.bind(this)} className="signUpForm">
                    <input onChange={this.props.handleInputChange} value={this.props.firstName} placeholder="First Name" type="text" name="firstName" className="signInput" id="userName"/>
                    <input onChange={this.props.handleInputChange} value={this.props.lastName} placeholder="Last Name" type="text" name="lastName" className="signInput" id="lastName"/>
                    <input onChange={this.props.handleInputChange} value={this.props.email} autoComplete="email" placeholder="E-mail" type="email" name="email" className="signInput" id="mail"/>
                    <input onChange={this.props.handleInputChange} value={this.props.password} autoComplete="none" placeholder="Create Password" type="password" name="password" className="signInput" id="password"/>
                    <input onChange={this.props.handleInputChange} value={this.props.passwordRe} autoComplete="none" placeholder="Re-type Password" type="password" name="passwordRe" className="signInput" id="passwordRe"/>
                    <input type="submit" value="NEXT" className="signInput" id="signSubmit"/>
                </form>
            </div>

        );
    }
}

export default SignField;
