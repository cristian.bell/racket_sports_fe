import React from 'react';
import {Link} from 'react-router-dom';
import Defs from '../Defs.js';
import DefFunctions from "../DefFunctions";


const TournamentItem = (props) =>{
  let defFunctions = new DefFunctions();

  const itemStyle={
    display: "flex",
    justifyContent: "space-around",
    textAlign: "center",
    margin: "0 auto !important",
    width: "100%",
    height: "100%",
    fontSize: "0.8rem",
    marginTop: "1%",
    backgroundColor: props.uId === props.tournamentData.creator_user_id ? 'rgba(255,200,200,0.3)' : 'rgba(255,255,255,0.2)'
  };
  const levelStyle={
    width: "20%",
    display: 'flex',
    alignItems: 'center', fontSize: '1.2rem'
  };
  const descStyle={
    width: "65%",
    fontWeight: "bold",

  };
  const imgStyle={
    display: 'flex',
    alignItems: 'center',
    width: "28%",
    marginRight: '1rem'

  };
  const EnrolledStyle={
    display: "flex",
    alignItems: 'center',
    textAlign: 'center',
    margin: '0 auto',
    fontWeight: 'bold',
    fontSize: '1.2rem'    
  };
  let startDate=String(props.tournamentData.start_date).slice(0,-5);
  let endDate=String(props.tournamentData.end_date).slice(0,-5);
  return(
      <div onClick={()=>console.log(props.uId, props.tournamentData.creator_user_id, (props.uId === props.tournamentData.creator_user_id), props.tournamentData)} style={itemStyle}>
        <div style={levelStyle}>
        	<p style={{margin: '0 auto'}}>
        	<Link style={{textDecoration: 'none', color: 'black'}} to={'/dynamicChart/'+props.tournamentData.id}>{defFunctions.levelToFloat(props.tournamentData.level)}</Link>
        	</p>
        </div>
        <div style={descStyle}>
        	<Link style={{textDecoration: 'none', color: 'black'}} to={'/home/TournamentView/'+props.tournamentData.id}>{Defs.tournamentGender[props.tournamentData.gender]}<br/>{startDate +"-"+ endDate} | {Defs.cities[props.tournamentData.location]}</Link>
        </div>
        <div style={imgStyle}>
          {/*<StackedImages imgArray={props.players}/>*/}
          <div style={EnrolledStyle}>{props.enrolledPlayers}/16</div>
        </div>
      </div>
  );
};
export default TournamentItem;
