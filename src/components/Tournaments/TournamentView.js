import React, {Component} from 'react';
import StackedImages from './StackedImages.js';
import BlackButton from './BlackButton.js';
import './TournamentView.css';
import Countdown from 'react-countdown-now';
import PlayerList from './PlayerList.js';
import readyIcon from './readyIcon.png';
import axios from 'axios';
import Defs from '../Defs.js';
import DefFunctions from '../DefFunctions.js';
import { Redirect} from 'react-router-dom';


class TournamentView extends Component{
  constructor(props){
    super(props);
      this.uId=sessionStorage.getItem('MPuId');
      this.openList=this.openList.bind(this);
      this.state = {
        levelOut: '',
        spotsLeft: 0,
        player: {},
        requestedPlayer: {},
        //imgArray: ["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg"],
        buttonContent: "JOIN (2 TICKETS)",
        tournamentStartTime: Date.now() + 604800000,  // will be created on Tournamentcreate
        joined: this.props.joined,
        playerList: false,
        owner: false,
        tournamentData: {},
        backToList: false,
        //succesPrompt: {open: false,message: 'succesfully joined tournament XXXX'},
        players: [{id: '60bh8f8mjcig',name: 'Hong Kong Tournament South', image: readyIcon},{id: '923239',name: 'N26 Bank Open', image: './readyIcon.png'},{id: '457123',name: 'Munich West Open', image: './readyIcon.png'},{id: '33333',name: 'Macau Venetian Tournament', image: './readyIcon.png'}]
    }
  }
  componentWillUpdate(){
    if(this.props.userObject.notificationToggle || this.props.userObject.optionToggle){
        this.props.closeOptions();
    }
}
  checkForId=(arr, check)=>{
    let passed= false;
    arr.forEach(element=>{
      if (element.id == check) {
        passed=true
      }
    });
    return passed;
  };
  componentDidMount(){
    let defFunctions = new DefFunctions();

    axios({
      method: 'get',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'tournament/'+this.props.match.params.id,
      withCredentials: true,
    }).then(response =>{
      console.log(response.data)
        this.setState({
            tournamentData: response.data,
            tournamentStartTime: response.data.start_time * 1000,
            owner: response.data.creator_user_id === this.props.userObject.uId,
            joined: this.checkForId(response.data.users, this.props.userObject.uId),
            levelOut: defFunctions.levelToFloat(response.data.level),
            spotsLeft: Defs.max_players - response.data.users.length,
            players: response.data.users
        });
    }).catch(response =>{
        //handle error
    });
  }
  openList(){
    this.setState({
      playerList: !this.state.playerList
    })
  }
  /* TODO make call with the id passed through router */
  joinTournament=()=>{
    //the id in the url
    axios({
      method: 'post',
      url: Defs.be_url + 'tournament/join/'+this.props.match.params.id,
      withCredentials: true,
      //data: formData,
    }).then(response =>{
      this.setState({joined: true});
      this.props.handleSnack("SUCCESFULLY JOINED "+this.state.tournamentData.name, 0, true,"UNDO",()=>this.leaveTournament())
       
    }).catch(response =>{
      this.props.handleSnack("FAILED TO JOIN ", 0, false)
        });
  };
  leaveTournament=()=>{
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'tournament/leave/'+this.props.match.params.id,
      withCredentials: true,
      //data: formData,
    }).then(response =>{
        this.setState({joined: false,
            succesPrompt: {open: true,message: "succesfully left tournament "+this.state.tournamentData.name}
        });
        this.props.handleSnack("SUCCESFULLY LEFT TOURNAMENT: "+this.state.tournamentData.name, 0, true,"UNDO",()=>this.joinTournament())
    }).catch(response =>{
      this.props.handleSnack("FAILED TO LEAVE ", 0, false)
        });
  };
  disableTournament=()=>{
      axios({
        method: 'delete',
        /*headers:{'id': this.props.match.params.id},*/
        url: Defs.be_url + 'tournament/' + this.props.match.params.id,
        withCredentials: true,
        //data: formData,
      }).then(response =>{
          this.setState({joined: false});
          this.props.handleSnack("SUCCESFULLY CANCELLED TOURNAMENT: " + this.state.tournamentData.name, 0, true)
      }).catch(response =>{
          //handle error
          this.props.handleSnack("SOMETHING WENT WRONG", 1, false)
          });
  };

  render() {
    if(this.state.backToList) return(<Redirect to={{
      pathname: '/home/TournamentFeed',
  }} />);

      return (
      <div className="TournamentContainer">
         {/*this.state.succesPrompt.open&&<div id='succesOverlay'>
          <p>{this.state.succesPrompt.message}</p>
          <div id='succesButtonCont'>
            <div onClick={()=>this.setState({backToList: true})}><p>BACK TO LIST!</p></div>
            <div onClick={()=>this.setState({succesPrompt :{open: false}})}><p>CLOSE</p></div>
          </div>
        </div>*/} 
        <div className="contentContainer">
          {this.state.playerList && <PlayerList players={this.state.players} close={this.openList}/>}

          <div id="tournamentDesc">
              <h1 id="skillHeader">{this.state.levelOut}</h1>
              <p id="desc">ADVANCED</p>

              <p><span className="thick">{Defs.tournamentGender[this.state.tournamentData.gender]}&nbsp;</span>{this.state.tournamentData.name}</p>
              <p><span style={{textDecoration: "underline"}} className="thick">{Defs.cities[this.state.tournamentData.location]}</span></p>
              <p><span className="thick">{this.state.tournamentData.start_date} </span> - <span className="thick">{this.state.tournamentData.end_date} </span></p>
              {<div onClick={this.openList}><div>LIST ALL PLAYERS</div></div>}
              <div id="secondaryInfo">
                <Countdown
                  date={this.state.tournamentStartTime}
                  renderer={props=> <div><span className="thick">{props.days}</span> DAYS, <span className="thick">{props.hours}</span> HOURS,<span className="thick">{props.minutes}</span>  MIN, <span className="thick">{props.seconds}</span> SEC</div>}
               />
                <p><span className="thick">1 </span>SET OF <span className="thick">{this.state.tournamentData.games} </span>  /  <span className="thick">{Defs.elim_rules_val[this.state.tournamentData.elim_rules]} </span></p>
                <p><span className="thick">{this.state.spotsLeft} SPOTS </span>LEFT</p>
              </div>
        </div>
        <div id="courtOptions">
          <div><p>{!this.state.tournamentData.courts ? 'self arranged' : 'provided'}</p></div>
        </div>
        {!this.state.joined && <div id="buttonDiv"><BlackButton buttonOnClick={this.joinTournament} content="JOIN"/></div>}
        {this.state.joined && <div id="buttonDiv"><BlackButton buttonOnClick={this.leaveTournament} content="WITHDRAW"/></div>}
        {this.state.owner && <div style={{marginTop: '2rem'}} id="buttonDiv"><BlackButton buttonOnClick={this.disableTournament} content="CANCEL"/></div>}
        </div>

      </div>
    );
  }
}

export default TournamentView;
