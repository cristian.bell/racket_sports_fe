import React from 'react';


const MatchItem = (props) =>{
  let color='rgba(255,0,0,0.5)';
  if (typeof props.match.outcome === 'string' && props.match.outcome === 'WIN') {
    color='rgba(0,255,0,0.5)'
  }
  const itemStyle={
    display: "flex",
    justifyContent: "space-around",
    textAlign: "center",
    margin: "0 auto !important",
    width: "100%",
    height: "80%",
    fontSize: "0.8rem",
    marginTop: "2%"
  }
  const dateStyle={
    width: "14%"
  }
  const outcomeStyle={
    color: color,
    width: "14%",
    fontWeight: "bold",

  }
  const partnerStyle={
    width: "22%",
    fontWeight: "bold"
  }
  const wideDivStyle={
    color: color,
    width: "22%"
  }

  /*const fieldStyle={
    width: "10%",
    height: "5vw",
    backgroundImage: "url("+court+")",
    backgroundSize: "cover",
    backgroundPosition: "center"
  }*/

  return(
      <div style={itemStyle}>
        <div style={dateStyle}>16. JUL{/*props.match.date*/}</div>
        <div style={outcomeStyle}>{props.match.outcome}</div>
        <div style={partnerStyle}>{props.match.partner}</div>
        <div style={wideDivStyle}>{props.match.location}</div>
        <div style={wideDivStyle}>{props.match.score}</div>
        {/*<div style={fieldStyle}>{props.match.field}</div>*/}
      </div>
  );
}
export default MatchItem;
