import React from 'react';
import TournamentItem from './TournamentItem';


const TournamentOverview = (props) =>{

    return(
    <div style={{ width: "100%"}}>
      <div style={{width: "20%",float: "left",textAlign: "center"}}>LEVEL</div><div style={{width: "50%",float: "left",textAlign: "center"}}>DETAILS</div><div style={{width: "28%",float: "left",textAlign: "center"}}>PLAYERS</div>
        {props.tournamentData.map(item=>  <div style={{paddingTop: "5%"}}><TournamentItem uId={props.uId} myTournaments={props.myTournaments} tournamentData={item} players={item.players} enrolledPlayers={item.nr_players}/></div>
 )}
    </div>
  );
}

export default TournamentOverview;
