import React from 'react';
import MatchItem from './MatchItem';


const SeasonList = (props) =>{
  let matchesWon=0;
  for(let i=0;i<props.matches.length;i++){
    if(props.matches[i].outcome === "WIN"){
      matchesWon++;
    }
  }
    return(
    <div>
      <p style={{textAlign: "center",fontSize: "90%",marginTop: "0%",marginBottom: "5%"}}>
      MATCHES PLAYED:  {props.matches.length}   W:  {matchesWon}   L:  {props.matches.length-matchesWon}
      </p>
      {props.matches.map(match=> <MatchItem match={match} />)}

    </div>
  );
  };

export default SeasonList;
