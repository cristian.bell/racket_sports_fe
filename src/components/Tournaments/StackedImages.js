import React from 'react';

const StackedImages = (props) =>(
    <div style={{zIndex: "1",height: "50%"}}  id="stackedImageDiv">
      {/*maps imgArray and spaces them evenly */}
      {props.imgArray.map((item,index)=> <div style={{left: index*(100/props.imgArray.length)+"%",backgroundImage: "url("+item+")", zIndex: "1"}} className="stackedPic"/>)}
    </div>
  )

export default StackedImages;
