import React, {Component} from 'react';

class TourBracket extends Component {
  constructor(props) {
    super(props);

    //let gameArr = [];
    this.state = {
      player: {},
      requestedPlayer: {},
      imgArray: [
        "X", "X", "X"
      ],
      buttonContent: "JOIN (2 TICKETS)"
    }
  }

  render() {
    const pHeight=14;
    const contStyle = {
      left: (this.props.data.startAX / 3.2 + 20) + "vw",
      top: (this.props.data.startAY / 3.2) + "vw",
      position: "absolute",
      height: this.props.data.height / 3.2 + "vw",
      width: this.props.data.length / 3.2 + "vw"
    }
    const matchStyle = {
      height: "100%",
      width: "60%",
      position: "absolute",
      borderTop: "1vw solid black",
      //borderBottom: "1vw solid black",
      borderRight: "1vw solid black",
      boxShadow: "inset 0 -10px 0 -2.1vw black"
    }
    const endLine = {
      position: "absolute",
      backgroundColor: "black",
      left: "60%",
      top: "50%",
      height: "1vw",
      width: "40%"
    }
    const playerOne = {
      position: "absolute",
      top: "-12%",
      margin: "0",
      paddingLeft: "5%",
      fontSize: "80%",
      color: "red"
    }
      const playerTwo = {
      position: "absolute",
      top: "101%",
      margin: "0",
      paddingLeft: "5%",
      fontSize: "80%",
      color: "blue"
    }

    const pOnePic={
      height: "14vw",
      width: "14vw",
      borderRadius: "50%",
      position: "absolute",
      marginLeft: (-pHeight/2)+"vw",
      marginTop: (-pHeight/2)+"vw",
      left: 0+"%",
      top: 0+"%",
      backgroundImage: "url(https://i.imgur.com/Tgqh5Hn.jpg?1)",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat"
    }
    const pTwoPic={
      height: "14vw",
      width: "14vw",
      borderRadius: "50%",
      position: "absolute",
      marginLeft: (-pHeight/2)+"vw",
      marginTop: (-pHeight/2)+"vw",
      left: 0+"%",
      top: 100+"%",
      backgroundImage: "url(https://i.imgur.com/GXHYdIG.jpg)",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat"
    }

    return (<div style={contStyle}>
      <p style={playerOne}/>

      <div style={matchStyle}>
        <div style={pOnePic} id="pOnePic"/>
        <div style={pTwoPic} id="pOnePic"/>
      </div>
      <p style={playerTwo}/>
      <div style={endLine}/>
    </div>);
  }
}

export default TourBracket;
