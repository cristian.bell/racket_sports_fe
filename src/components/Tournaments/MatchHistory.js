import React, {Component} from 'react';
import SeasonList from './SeasonList.js';
import './History.css';


class MatchHistory extends Component{
  constructor(props){
    super(props);
      this.state = {
        player: {},
        requestedPlayer: {},
        imgArray: ["X","X","X"],
        buttonContent: "JOIN (2 TICKETS)",
        matches: [{date: "24.4",outcome: "WIN",partner: "H. CONTRA",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "12.5",outcome: "LOSE",partner: "A. ONG",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "13.5",outcome: "WIN",partner: "Y. MING",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "25.6",outcome: "LOSE",partner: "O. MING",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "1.7",outcome: "LOSE",partner: "U. ESTRA",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "12.7",outcome: "WIN",partner: "M. NYQUIST",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "24.7",outcome: "LOSE",partner: "U. TRIX",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "4.8",outcome: "LOSE",partner: "S. YUNG",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "14.8",outcome: "LOSE",partner: "T. ONG",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "21.8",outcome: "WIN",partner: "B. UEKA",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "4.9",outcome: "LOSE",partner: "E. RICO",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "10.9",outcome: "LOSE",partner: "J. COPELLA",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "14.10",outcome: "LOSE",partner: "O. ONG",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "16.10",outcome: "LOSE",partner: "K. DAILY",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "9.11",outcome: "LOSE",partner: "I. ELISI",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "3.12",outcome: "LOSE",partner: "N. TUCO",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"},
        {date: "5.12",outcome: "LOSE",partner: "G. YUNG",location: "BERLIN M.",score: "6-0",color: "200,100,100,0.5"}],
        matchesWon: 7,
    }


  }
  componentDidMount(){

  }


  render() {
    return (
      <div style={{marginTop: "10%"}}>
        <div className="">
          <div id="historyContent" >
            <h1>WINTER 2018</h1>
            <h1>SEPT 2018-MARCH 2019</h1>
          <div id="historyDiv">
            <SeasonList matches={this.state.matches}/>
          </div>
          </div>
          </div>

      </div>
    );
  }
}

export default MatchHistory;
