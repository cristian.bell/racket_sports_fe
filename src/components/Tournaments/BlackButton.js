import React from 'react';



const BlackButton = (props) =>{
  const buttonStyle={
    display: "flex",
    textAlign: "center",
    margin: "0 auto !important",
    width: "100%",
    height: "100%",
    backgroundColor: "black",
    color: "white",
    borderRadius: "2vw",
    border: "none",
    alignItems: "center"
  }
  const pStyle={
    margin: "0 auto",
    fontSize: "1.4rem",
    fontWeight: "bold"
  }
  return(
      <button onClick={props.buttonOnClick} style={buttonStyle}><p style={pStyle}>{props.content}</p></button>
  );
}
export default BlackButton;
