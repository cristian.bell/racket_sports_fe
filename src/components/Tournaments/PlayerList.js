import React, {Component} from 'react';
import ProfileImage from '../profile/ProfileImage.js';
import {Link} from 'react-router-dom';
import placeholderImage from '../user.png'


class PlayerList extends Component{
  constructor(props){
    super(props);
      this.state = {
        players: [{name: "PLAYER ONE"},{name: "PLAYER TWO"},{name: "PLAYER THREE"},{name: "PLAYER FOUR"},{name: "PLAYER ONE"},{name: "PLAYER TWO"},{name: "PLAYER THREE"},{name: "PLAYER FOUR"}]
    }
  }


  render() {
    const contStyle={
      zIndex: '8',
      position: 'absolute',
      top: '10%',
      left: '5%',
      width: '90%',
      height: "80%",
      backgroundColor: 'white',
      display: 'flex',
      //alignItems: 'center',
      overflow: 'auto'

    }
    const playerItem={
      margin: '5% auto',
      textAlign: 'center',
      width: '80%',
      height: '5%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around'
    }
    const innerDiv={
      marginTop: '15%',
      marginBottom: '10%',
      width: '100%',

    }
    const closeIt={
        position: 'fixed',
        right: "8%",
        top: '12%',
        fontSize: '2rem'
    }
    return (
      <div style={contStyle}>
        <div onClick={this.props.close} style={closeIt}><i class="fas fa-times"/></div>
        <div style={innerDiv}>
          {this.props.players.map(player => <Link to={'/home/Profile/'+player.id}><div style={playerItem}><div style={{width: '30%'}}><ProfileImage source={player.image || placeholderImage}/></div><div style={{width: '70%'}}>{player.firstname+" "+player.lastname}</div></div></Link>)}
        </div>

      </div>
    );
  }
}

export default PlayerList;
