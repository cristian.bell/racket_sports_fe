import React, {Component} from 'react';
import './ChartStyle.css';
import TourBracket from './TourBracket';

class TournamentChart extends Component{
  constructor(props){
    super(props);
    this.createTree=this.createTree.bind(this);
    this.Match=this.Match.bind(this);
    this.stufflol=this.createTree(16,20,50,50);
      this.state = {
        player: {},
        requestedPlayer: {},
        imgArray: ["X","X","X"],
        buttonContent: "JOIN (2 TICKETS)",
        gameChart: []
    }
  }

  createTree(players,startTreeX,startTreeY,gap){

  this.isPowerOfTwo=false;
  this.col=Math.ceil(Math.sqrt(players,2));
  this.gap=gap;
  this.gameArr=Array(this.col);
  this.flatOutPut=[];
  if(players && (players & (players - 1)) === 0){
    this.isPowerOfTwo=true;
  }
  for(let i=0;i<this.gameArr.length;i++){
    this.gameArr[i]=Array(Math.pow(2,i));
  }
  for(let i=this.gameArr.length-1;i>=0;i--){
    for(let j=0;j<this.gameArr[i].length;j++){
        if(i === this.gameArr.length-1){
          if(j === 0){
            this.gameArr[i][j]=new this.Match(startTreeX,startTreeY,startTreeX,140,140,90);
            this.flatOutPut.push(this.gameArr[i][j]);
          }else{
            this.gameArr[i][j]=new this.Match(this.gameArr[i][j-1].startAX,this.gameArr[i][j-1].startBY+this.gap,this.gameArr[i][j-1].startBX,this.gameArr[i][j-1].startBY+this.gap+this.gameArr[i][j-1].height,140,0);
            this.flatOutPut.push(this.gameArr[i][j]);
          }
          }else{
          this.gameArr[i][j]=new this.Match(this.gameArr[i+1][j].endX,this.gameArr[i+1][2*j].endY,this.gameArr[i+1][2*j+1].endX,this.gameArr[i+1][2*j+1].endY,140,0);
          this.flatOutPut.push(this.gameArr[i][j]);
      }
    }
  }
}


Match(startAX, startAY, startBX, startBY, length, middle){
  this.height=startBY-startAY;
  this.length=length;
  this.next=startBY+20;
  this.middle=(startBY+startAY)/2;
  this.endX=startAX+length;
  this.endY=(startBY+startAY)/2;
  this.startAX=startAX;
  this.startAY=startAY;
  this.startBX=startBX;
  this.startBY=startBY;

}

  render() {
    return (
      <div style={{height: window.innerHeight}} className="ChartContainer">
        <div style={{marginTop: "20%"}} id="chartContent">
          {this.flatOutPut.map(match=> <TourBracket data={match}/>)}
        </div>

      </div>
    );
  }
}

export default TournamentChart;
