import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import TournamentOverview from './TournamentOverview.js';
import Defs from '../Defs.js';
import './TournamentView.css';
import './tSum.css';


class TournamentSum extends Component{
  constructor(props){
    super(props);
      this.state = {
        tournamentData:[{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},{data:{},players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},],
        imgArray: ["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"],

    }
  }
  componentDidMount(){
    axios.get(Defs.be_url + "match/list/")
    .then(response =>{
        const userData=response.data;
        this.setState({userData});
          this.setState({
          })
        })
        .catch(response =>{
          console.log(response.status)
        });
  }


  render() {
    return (
      <div className="TournamentContainer">
        <div id="sumContainer" className="contentContainer">

          <div id="tournamentSum">
            <TournamentOverview tournamentData={this.state.tournamentData}/>
          </div>
          <div id="moreTournaments">
            <div id="tournamentOptions">
              <Link to="../createTournament"><div id="bigPlus"><p>+</p></div></Link>
              <div id="myTournaments"><p>MY<br/><br/> TOURNAMENTS</p></div>
            </div>
          </div>

        </div>


      </div>
    );
  }
}

export default TournamentSum;
