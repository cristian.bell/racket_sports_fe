import React, {Component} from 'react';


class PlayStyle extends Component{
  constructor(props){
    super(props);
      this.state = {
    }

  }

  render() {
    const contStyle={
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    }
    const labelCol={
      width: "40%",

    }
    const inputCol={
      width: "55%",

    }
    const inputStyle={
      margin: "0",
      padding: "0",
      display: "block",
      margin: "0 auto"
    }
    const checkBox={
      width: "5%",

    }
    return (
      <div style={contStyle}>
        <div style={labelCol}>{this.props.label}</div>
        <div style={inputCol}><input style={inputStyle} type={this.props.type} value={this.props.value} name={this.props.name} onChange={this.props.onChange}/></div>
        <div style={checkBox}>{this.props.checkBox &&<input onChange={this.props.onChange} checked={this.props.check} name={this.props.name+"Check"}  type="checkBox"/>}</div>
      </div>
    );
  }
}

export default PlayStyle;
