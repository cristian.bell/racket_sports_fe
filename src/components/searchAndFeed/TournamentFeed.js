import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './tFeed.css';
import TournamentOverview from '../Tournaments/TournamentOverview.js';
import axios from 'axios';
import Defs from '../Defs.js';


class TournamentFeed extends Component{
  constructor(props){
      super(props);
      let dataTournament = [
      {id: 222,location: Defs.cities[0],level: '4.5',dateRange: "NOV 12-NOV 28", gender: "MENS SINGLES"},
	  {id: 1234,location: Defs.cities[1],level: '4.5',dateRange: "OCT 16-OCT 23", gender: "WOMENS SINGLES"},
	  {id: 1235,location: Defs.cities[2],level: '5.0',dateRange: "NOV 16-NOV 23", gender: "MEN SINGLES"},
	  {id: 1235,location: Defs.cities[3],level: '3.5',dateRange: "DEC 03-DEC 17", gender: "MEN SINGLES"},
	  {id: 1236,location: Defs.cities[4],level: '4.0',dateRange: "OCT 22-OCT 29", gender: "WOMENS SINGLES"},
	  {id: 125,location: Defs.cities[5],level: '5.5',dateRange: "DEC 03-DEC 17", gender: "MEN SINGLES"}
	  ];

      this.setAll=this.setAll.bind(this);
      this.setMy=this.setMy.bind(this);
      this.toggleOptions=this.toggleOptions.bind(this);
      this.state = {
         tabAll: "hasUnderline",
         tabMy: "noUnderline",
         tabOpen: "my",
         tournamentData: [],
         userTournamentData: [],
         allData:[
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
         {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},

         ],
         imgArray: ["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"],

         myData:[
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},
          {enrolledPlayers:getRandomInt(dataTournament.length),data:dataTournament[getRandomInt(dataTournament.length)], players:["http://tremendouswallpapers.com/wp-content/uploads/2015/02/sinary_bhs35wjr.jpg","https://i.imgur.com/cSOyQhy.jpg","https://i.imgur.com/GXHYdIG.jpg"]},

         ],

         toggleOptions: false
    };
    function getRandomInt(max) {
  		return Math.floor(Math.random() * Math.floor(max));
	}
}


componentWillUpdate(){
  if(this.props.userObject.notificationToggle || this.props.userObject.optionToggle){
      this.props.closeOptions();
  }
}
setAll(){
  this.setState({
    tabAll: "noUnderline",
    tabMy: "hasUnderline",
    tabOpen: "all"
  })
}
setMy(){
  this.setState({
    tabMy: "noUnderline",
    tabAll: "hasUnderline",
    tabOpen: "my"
  })
}

toggleOptions(){
  this.setState({
    toggleOptions: !this.state.toggleOptions
  })
}

componentDidMount(){
  this.props.navIndex(2);
      axios.all([
        axios.get(Defs.be_url + 'tournament/list/', {withCredentials:true}),
        axios.get(Defs.be_url + 'tournament/by_user/' + this.props.uId, {withCredentials:true})
      ])
      .then(axios.spread((all, users) => {
          const tournamentData=all.data;
          const userTournamentData=users.data;
          const userOnTop=userTournamentData.reduce((acc, element)=>{
          if (parseInt(element.enrolledPlayers, 10) === 2) {
            return [element, ...acc]
          }return [...acc, element];
        },[]);
          this.setState({allData: tournamentData,myData: userOnTop});
          //console.log(this.state.tournamentData, this.state.userTournamentData)
      }));
}
render() {
  return (
    <div className="feedContainer">
      <div id="navRow">
        <div onClick={this.setAll} id="allTour" className={this.state.tabAll}>{/*<div onClick={this.toggleOptions} id="searchOptionDiv"><i id="settingsIcon" className="fas fa-sliders-h"></i></div>*/}<p>ALL</p></div>
        <div onClick={this.setMy} id="myTour" className={this.state.tabMy}><p>MY TOURNAMENTS</p></div>
      </div>
      {this.state.toggleOptions && <div id="optionSlideOut"/>}
      <div id="feedContent">
        <Link to="/home/createTournament">
          <div id="createButton">
            <i className="fas fa-plus"/>
          </div>
        </Link>
        {this.state.tabOpen==="all" && <TournamentOverview tournamentData={this.state.allData} myTournaments={false} uId={this.props.uId}/>}
        {this.state.tabOpen==="my" && <TournamentOverview tournamentData={this.state.myData} myTournaments={true} uId={this.props.uId}/>}
      </div>
    </div>
    );
  }
}

export default TournamentFeed;
