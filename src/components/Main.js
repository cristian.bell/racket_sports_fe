import React, {Component} from 'react';
import {Redirect, Switch, Route} from 'react-router-dom';
import LogIn from './LogIn';
import SignUp from './SignUp';
import Welcome from './Welcome';
import SignUpFinished from './Register/SignUpFinished';
import TournamentView from './Tournaments/TournamentView';
import UserSite from './profile/UserSite';
import CreateTournament from './createTournament/CreateTournament';
import DynamicChart from './TournamentChart/DynamicChart';
import TournamentFeed from './searchAndFeed/TournamentFeed.js';
import ProfilePublic from './profile/ProfilePublic';

import GeneralRules from './GeneralRules.js';

class Main extends Component{

  constructor(props){
    console.log("Main() called");
    super(props);
      this.state = {
      isLoggedIn: true
    }
}

handleLogIn(event){
  this.setState({
    isLoggedIn: true
  });
}

handleLogOut(event){
  this.setState({
    isLoggedIn: false
  });
}
render(){
    console.log('logInState: ' + this.props.logInState);
  return (
    <main>

      <Switch>
      //@todo is this still used?
        {this.props.logInState? <Route exact path='/' render={(props) => <UserSite {...props} logMeOut={this.props.logOut} />}/> : <Route exact path='/' component={Welcome}/>}
        <Route path='/logIn' render={(props) => <LogIn logInState={this.props.logInState} logMeIn={this.props.logIn} />}/>
        <Route path='/signUp' render={(props) => <SignUp logMeIn={this.props.logIn} />}/>
        {/*<Route path='/home/editProfile' render={(props) => <EditProfile {...props}/>}/>*/}
        {/*<Route path='/editProfile' render={(props) => <EditProfile {...props}/>}/>*/}
        <Route path='/TournamentFeed' render={(props) => <TournamentFeed logMeIn={this.props.logIn} />}/>
        <Route path='/DynamicChart' render={(props) => <DynamicChart logInState={this.props.logInState} logMeIn={this.props.logIn} />}/>
        <Route path='/SignUpFinished' render={(props) => <SignUpFinished logInState={this.props.logInState} logMeIn={this.props.logIn} />}/>
        <Route path='/createTournament' render={(props) => <CreateTournament logInState={this.props.logInState} logMeIn={this.props.logIn} />}/>
        <Route path='/profile/:id' render={(props) => <ProfilePublic {...props} />}/>
        <Route path='/GeneralRules' render={(props) => <GeneralRules {...props} />}/>
        <Route path='/tournamentView/:id' render={(props) => <TournamentView {...props}/>}/>
        {this.props.logInState? <Route path='/home' render={(props) => <UserSite {...props} logMeOut={this.props.logOut} />}/> : <Redirect to={'/LogIn'}/>}
        {/*{this.state.isLoggedIn && <Route path='/TournamentSum' render={()=><TournamentSum {...this.state}/>}/>}}
        {this.state.isLoggedIn && <Route path='/TournamentView' render={()=><TournamentView {...this.state}/>}/>}
        {this.state.isLoggedIn && <Route path='/MatchHistory' render={()=><MatchHistory {...this.state}/>}/>}
        {this.state.isLoggedIn && <Route path='/Profile' render={()=><Profile {...this.state}/>}/>}
        {this.state.isLoggedIn && <Route path='/TournamentChart' render={()=><TournamentChart {...this.state}/>}/>}
        */}
      </Switch>

    </main>

    );
  }
}
export default Main;
