import React from 'react';
import './SignUp.css';

const UstInfo = () =>(


  <div>

  <div id="USToverlay" className="overlay"><div id="ustText">1.0 This player is just starting to play tennis.

  <br /><br />1.5 This player has limited playing experience and is still working primarily on getting the ball into play.

  <br /><br />2.0 This player needs on-court experience. This player has obvious stroke weaknesses but is familiar with basic positions for singles and doubles play.

  <br /><br />2.5 This player is learning to judge where the ball is going although court coverage is weak. This player can sustain a slow rally with other players of same ability.

  <br /><br />3.0 This player is consistent when hitting medium pace shots, but is not comfortable with all strokes and lacks control when trying for directional intent, depth, or power.

  <br /><br />3.5 This player has achieved improved stroke dependability and direction on moderate pace shots, but still lacks depth and variety. This player exhibits more aggressive net play, has improved court coverage and is developing teamwork in doubles.

  <br /><br />4.0 This player has dependable strokes, including directional intent, on both forehand and backhand sides on moderate shots, plus the ability to use lobs, overheads, approach shots and volleys with some success. This player occasionally forces errors when serving and teamwork in doubles is evident.

  <br /><br />4.5 This player has begun to master the use of power and spins and is beginning to handle pace, has sound footwork, can control depth of shots, and is beginning to vary tactics according to opponents. This player can hit first serves with power and accuracy and place the second serve and is able to rush the net successfully.

  <br /><br />5.0 This player has good shot anticipation and frequently has an outstanding shot or attribute around which a game may be structured. This player can regularly hit winners or force errors off of short balls, can put away volleys, can successfully execute lobs, drop shots, half volleys and overhead smashes, and has good depth and spin on most second serves.

  <br /><br />5.5 This player has developed power and/or consistency as a major weapon. This player can vary strategies and styles of play in a competitive situation and hits dependable shots in a stress situation.

  <br /><br />6.0 These players will generally not need NRTP rankings. Rankings or past rankings will speak for themselves. The 6.0 player typically has had intensive training for national tournament competition at the junior level and collegiate levels and has obtained a sectional or national ranking.

  <br /><br />The 6.5 player has a reasonable chance of succeeding at <br /><br/>the 7.0 level and has extensive satellite tournament experience.

  <br /><br />The 7.0 is a world class player who is committed to tournament competition on the international level and whose major source of income is tournament prize winnings.
</div>
  </div>
  </div>

)

export default UstInfo;
