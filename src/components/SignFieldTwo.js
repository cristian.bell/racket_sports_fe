import React, {Component} from 'react';
import { Redirect} from 'react-router-dom';
import DefFunctions from './DefFunctions.js';
import './SignUp.css';
import Defs from "./Defs";

class SignFieldTwo extends Component{
    constructor(props){
        super(props);
        this.state = {
            backToStep1: false,
            backToStep2: false,
            backToStep3: false,
            signUpFinished : false,
            fieldError: false,
            ustaInfoMessage : ["Beginner","Beginner","Beginner","Beginner","Adv. Beginner","Intermediate","Adv. Intermediate","Competitor","Adv. Competitor","Expert","Adv. Expert","Tournament Player","Tournament Player","Professional Player","Professional Player"]
        }
    }
    
    finishStage(event){
        event.preventDefault();
        let defFunctions = new DefFunctions();
        if (defFunctions.checkFormDataStep1(this.props, this) !== true) {
            this.setState({
                signUpFinished: false,
                backToStep1: true,
                fieldError: true,
            });
            return false;
        }

        if (defFunctions.checkFormDataStep2(this.props, this) !== true) {
            this.setState({
                signUpFinished: false,
                fieldError: true,
            });
            return false;
        }

        this.setState({
            signUpFinished: true
        });
    }

    render() {
        if(this.state.backToStep1) return(<Redirect to={{
            pathname: '/signUp',
        }} />);
        return (
            <div className="signUpContain">
                {((Defs.formError)) && <div className="passwordError">{Defs.formErrorMessage}</div>}
                {this.state.signUpFinished && (<Redirect to={'/signUp/SignFieldThree'}/>)}
                <form  onSubmit={this.finishStage.bind(this)} className="signUpFormTwo">
                    <p className="inputsP" id="genderP">GENDER</p>
                    <div>
                        {this.props.genderChoice.map((gender, i) =>
                            <div className="radios">
                                <input onChange={this.props.handleInputChange} name="gender" value={i+1}
                                       checked={parseInt(this.props.gender, 10) === i+1} type="radio"
                                       className="genderRadio" id={gender + 'radio'} />
                                <label id={gender} className="genderLabel" htmlFor={gender + 'radio'}><p>{gender}</p></label>
                            </div>
                            )}
                    </div>
                    <div className="inputs">
                        <p  className="inputsP" id="birth">AGE</p>
                        <div style={{paddingTop: '1rem'}} className="radios" id="ageRadios">
                            <input value="1" onChange={this.props.handleInputChange} checked={this.props.ageGroup === "1"}  type="radio" name="ageGroup" className="playFrequRadio" id="young"/>
                            <label id="young" className="ageLabel" htmlFor="young"><p>&lt; 18</p></label>
                            <input value="2" onChange={this.props.handleInputChange} checked={this.props.ageGroup === "2"} type="radio" name="ageGroup" className="playFrequRadio" id="notYoung"/>
                            <label id="notOften" className="ageLabel" htmlFor="notYoung"><p>18 - 34</p></label>
                            <input value="3" onChange={this.props.handleInputChange} checked={this.props.ageGroup === "3"}  type="radio" name="ageGroup" className="playFrequRadio" id="old"/>
                            <label id="often" className="ageLabel" htmlFor="old"><p>34 - 50</p></label>
                            <input value="4" onChange={this.props.handleInputChange} checked={this.props.ageGroup === "4"} type="radio" name="ageGroup" className="playFrequRadio" id="veryOld"/>
                            <label id="superOften" className="ageLabel" htmlFor="veryOld"><p>50 +</p></label>
                        </div>
                        <p className="inputsP" id="city">YOUR CITY</p>
                        <select onChange={this.props.handleInputChange} name="city" className="signInputTwo" >{this.props.cityChoice.map((city,i) => <option value={i} key={i}>{city}</option>)}</select>

                        <p id="lvlP" className="lvl">LEVEL: <span id="skillLvlSpan">{this.props.lvlSlider / 10}</span>
                            <span id="ustaInfoMessage">{this.state.ustaInfoMessage[this.props.lvlSlider / 5]}</span>
                        </p>
                        <p className="lvl">0.0</p>

                        <p className="lvl" id="seven">7.0</p>

                        <input step="5" onChange={this.props.handleInputChange} name="lvlSlider" type="range" min="0" max="70" value={this.props.lvlSlider} className="slider"/>
                        <p className="lvl" id="lvlDesc">"Based on UST Guide"</p>
                        <a id="ustWhat" href="#" onClick={this.props.toggleIt}>What is USTA?</a>
                    </div>
                    <input type="submit" value="NEXT" className="signInput" id="signSubmit"/>
                </form>

            </div>

        );
    }
}

export default SignFieldTwo;
