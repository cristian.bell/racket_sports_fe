
import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={8}
    defaultCenter={{ lat: props.myLat, lng: props.myLong }}
  >
    {props.isMarkerShown && <Marker position={{ lat: 52.520008, lng: 13.40495 }} />}
  </GoogleMap>
))
export default MyMapComponent;
