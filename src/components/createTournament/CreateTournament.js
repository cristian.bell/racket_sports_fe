import React, {Component} from 'react';
//import { Redirect} from 'react-router-dom';
//import { browserHistory } from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import Calendar from 'react-calendar';
//import CalenderOverlay from './CalenderOverlay.js';
//import MyMapComponent from './MyMapComponent.js'
import './createTournament.css';
import NavFooter from '../profile/NavFooter';
//import Geosuggest from 'react-geosuggest-plus';
import axios from 'axios';
import Defs from '../Defs.js';
import SnackBar from '../helper/SnackBar.js';

class CreateTournament extends Component{
  constructor(props){
      super(props);
      this.handleSubmit=this.handleSubmit.bind(this);
      this.handleInputChange=this.handleInputChange.bind(this);
      this.changeDate=this.changeDate.bind(this);
      this.openCalenderStart=this.openCalenderStart.bind(this);
      this.openCalenderEnd=this.openCalenderEnd.bind(this);
      this.closeCalender=this.closeCalender.bind(this);
      this.lowestDate=new Date();
      this.lowestDate.setDate(this.lowestDate.getDate()+8)
      this.highestDate=new Date();
      this.highestDate.setMonth(this.highestDate.getMonth()+1);
       this.state = {
        tournamentName: '',
        signUpFinished : false,
        gender: "0",
        matches: "0",
        lvlSlider: "50",
        rules: "0",
        sets: 1,
        setsOf: 6,
        courts: "0",
        typeInfo: ["MINIMUM OF 4 PLAYERS REQUIRED MAX 16","MINIMUM OF 4 PLAYERS REQUIRED MAX 16","MINIMUM OF 4 PLAYERS REQUIRED MAX 16"],
        comingSoon: true,
        myDate: [new Date(), new Date()],
        startOpen:false,
        endOpen: false,
        location: 0,
        monthNames:["ZeroMonth","JAN","FEB","MAR","APR","JUN","JUL","AUG","SEP","OCT","NOV","DEC"],
        cityChoice: Defs.cities,
        redirectToTournament: {doRedirect: false, id: ''}
    }
}
finishStage(event){
  event.preventDefault();
  this.handleSubmit(event);
  this.setState({
    signUpFinished: true
  });
}
handleInputChange(event) {
  const target = event.target;
  const value = target.type === 'checkbox' ? target.checked : target.value;
  const name = target.name;
  this.setState({
    [name]: value
  });

}
    handleSubmit(event){
        event.preventDefault();
        const formData=new FormData();
        formData.append("name", this.state.tournamentName);
        formData.append("location", parseInt(this.state.location));
        formData.append("start_date",this.state.myDate[0].toString());
        formData.append("end_date", this.state.myDate[1].toString());
        formData.append("courts", this.state.courts);
        formData.append("gender", this.state.gender);
        formData.append("level", this.state.lvlSlider/10);
        formData.append("elim_rules", this.state.rules);
        formData.append("sets", this.state.setsOf);
        console.log(this.state.lvlSlider/10)
        axios({ method: 'post', url: Defs.be_url + 'tournament'/*,headers:{'x-api-key': '123'}*/,
            data: formData, withCredentials:true})
    .then(response =>{
            
            if(response.status === 200){
              this.props.handleSnack('TOURNAMENT SUCCESFULLY CREATED', 0, false)
              this.setState({redirectToTournament: {doRedirect: true, id: response.data.id}})
            }
        })
    .catch(response =>{
                console.log(response);
                this.props.handleSnack('SOMETHING WENT WRONG :(', 1, false)
            });
    }
    changeDate(myDate){
  this.setState({ myDate }, () => {
    console.log(this.state.myDate[1]);
  });

}

openCalenderStart(){
  this.setState({
    endOpen: false,
    startOpen: true
  })
}
openCalenderEnd(){
  this.setState({
    startOpen: false,
    endOpen: true
  })
}
closeCalender(){
  this.setState({
    startOpen: false,
    endOpen: false
  })
}



render() {
  if(this.state.redirectToTournament.doRedirect) return(<Redirect to={{
    pathname: '/home/TournamentView/'+this.state.redirectToTournament.id,
}} />);
  const fixLink={
    textDecoration: "none",
    color: "black"
  };
    return (
    <div style={{height: window.innerHeight+"px"}} id="createPage">
      {/* <Link style={fixLink} to={'/home'}><div className="closeButton"><i className="fas fa-times"/></div></Link> */}
      <div className="createTourCont">
        <form  onSubmit={this.handleSubmit.bind(this)} className="createTourForm">
          <input onChange={this.handleInputChange} name='tournamentName'  type='text' value={this.state.tournamentName} placeholder='TOURNAMENT NAME' id='tournamentNameField'></input>
          <p className="inputsP" id="backHandP">MATCHES</p>
          <div className="radiosT">
            <input value="1" onChange={this.handleInputChange} checked={this.state.matches === "1"} type="radio" name="matches" className="handRadio" id="rightMatches"/>
            <label id="male" className="bigLabel" htmlFor="rightMatches"><p>SINGLES</p></label>
            <input value="2"  checked={this.state.matches === "2"} type="radio" name="matches" className="handRadio" id="leftMatches"/>
            <label id="comingSoon" className="bigLabel" htmlFor="leftMatches"><p className="disabledP">DOUBLES</p>{this.state.comingSoon && <p className="comingSoonDoubles">coming soon</p>}</label>
          </div>
          <p className="inputsP" id="handP">GENDER</p>
          <div className="radiosT">
            <input value="1" onChange={this.handleInputChange} checked={this.state.gender === "1"}  type="radio" name="gender" className="handRadio" id="rightRadio"/>
            <label id="" className="smallLabel" htmlFor="rightRadio"><p>MEN</p></label>
            <input value="2" onChange={this.handleInputChange} checked={this.state.gender === "2"} type="radio" name="gender" className="handRadio" id="midRadio"/>
            <label id="" className="smallLabel" htmlFor="midRadio"><p>WOMEN</p></label>
              <input value="3" checked={this.state.gender === "3"} type="radio" name="gender" className="handRadio" id="leftRadio"/>
              <label id="comingSoon" className="smallLabel" htmlFor="leftRadio">{this.state.comingSoon && <p className="comingSoonMixed">coming soon</p>}<p className="disabledP">MIXED</p></label>
          </div>
          <div id="sliderDiv">
            <p id="lvlP" className="lvl">LEVEL: <span id="skillLvlSpan">{this.state.lvlSlider/10}</span><span id="ustaInfoMessage">{this.state.ustaInfoMessage}</span></p>
              <p className="lvl">0.0</p>
              <p className="lvl" id="seven">7.0</p>
            <input step="5" onChange={this.handleInputChange} name="lvlSlider" type="range" min="0" max="70" value={this.state.lvlSlider} className="slider"/>
            <p className="lvl" id="lvlDesc">PLAYERS WITH LVLS:  {this.state.lvlSlider>15?(this.state.lvlSlider/10)-1.5:0}-{this.state.lvlSlider<65?(this.state.lvlSlider/10)+1.5:7}</p>
          </div>

          <p className="inputsP" id="freqP">RULES</p>
            <div className="radiosT">
              <input value="1" onChange={this.handleInputChange} checked={this.state.rules === "1"}  type="radio" name="rules" className="handRadio" id="leftRule"/>
              <label id="singles" className="bigLabel" htmlFor="leftRule"><p>KNOCK OUT</p></label>
              <input value="2" onChange={this.handleInputChange} checked={this.state.rules === "2"} type="radio" name="rules" className="handRadio" id="rightRule"/>
              <label id="doubles" className="bigLabel" htmlFor="rightRule"><p>ROUND ROBIN</p></label>
            </div>
            <div id="typeInfoDiv"><p id="typeInfo">{this.state.typeInfo[this.state.rules]}</p></div>
            <p className="inputsP" id="freqP">DATE</p>
            <div id="showDate">
              <div id="dateFrom">
                <div >FROM:  {this.state.myDate[0].getUTCDate()+1}:{this.state.monthNames[this.state.myDate[0].getUTCMonth()]}:{this.state.myDate[0].getUTCFullYear()}</div>
              </div>
              <div id="dateTo">
                <div >TO:  {this.state.myDate[1].getUTCDate()+1}:{this.state.monthNames[this.state.myDate[1].getMonth()]}:{this.state.myDate[1].getUTCFullYear()}</div>
              </div>
            </div>
            <div className="calendarTab">
              <Calendar
                selectRange={true}
                minDate={this.lowestDate}
                maxDate={this.highestDate}
                onChange={this.changeDate}
                value={this.state.myDate}
              />

            </div>

            <div id="setsDiv">
              {/* <input value={this.state.sets} name="sets" /*onChange={this.handleInputChange} type="number"/> */}
              <div>1</div>
              <p>SET OF</p>
              <input value={this.state.setsOf} name="setsOf" onChange={this.handleInputChange} type="number"/>
            </div>

            <p className="inputsP" id="freqP">COURTS</p>
              <div className="radiosT">
                <input value="1" onChange={this.handleInputChange} checked={this.state.courts === "1"}  type="radio" name="courts" className="handRadio" id="leftCourts"/>
                <label id="male" className="bigLabel" htmlFor="leftCourts"><p>PROVIDED</p></label>
                <input value="2" onChange={this.handleInputChange} checked={this.state.courts === "2"} type="radio" name="courts" className="handRadio" id="rightCourts"/>
                <label id="female" className="bigLabel" htmlFor="rightCourts"><p>SELF ARRANGED</p></label>
            </div>
            <div id="mapDiv">
              <div id="locationDiv">
                    <p id="locationHead">LOCATION</p>
                    <select onChange={this.handleInputChange} name="location" className="signInputTwo" >{this.state.cityChoice.map((city,i) => <option value={i} key={i}>{city}</option>)}</select>
              </div>
            </div>


        <div id="submitWrapper"><input type="submit" value="CREATE" className="signInput" id="signSubmitTour"/></div>
      </form>

  </div>

</div>
    );
  }
}

export default CreateTournament;
