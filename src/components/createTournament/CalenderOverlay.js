import React, {Component} from 'react';
import Calendar from 'react-calendar';

import './createTournament.css';

class CalenderOverlay extends Component{
  constructor(props){
      super(props);

       this.state = {



    }
}


render() {
    return (
    <div id="calenderOverlay">
     <div id="calenderHeading"><p>{this.props.desc}</p></div>
     <div onClick={this.props.closeIt} id="closeIcon">X</div>
      <Calendar
      
        onChange={this.props.changeDate}
        value={this.props.date}
      />
    </div>
    );
  }
}

export default CalenderOverlay;
