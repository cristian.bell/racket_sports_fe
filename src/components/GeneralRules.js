import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './GeneralRules.css'; //Maybe ._.

class GeneralRules extends Component{

  constructor(props){
    super(props);
    this.containerRef=React.createRef();
    //this.userObject=this.props.location.state.reference;
    console.log(this.containerRef);
  }
  componentDidMount(){
    console.log(this.containerRef + ' MMMMounted');
  }
  render(){
      const generalRulesStyling={
        height: "100%",
        width: "90%",
        margin:"auto"
      }
      const fixLink={
        textDecoration: "none",
        color: "black"
      }
      const makeBold={
        textDecoration:"underline",
        marginTop:"20%",
        fontSize:"1.4rem"
      }
    return(
      <div style={{height: window.innerHeight +"px"}} id="profileComp">
        {/*<div ref={this.containerRef} style={generalRulesStyling}>*/}
        <div style={generalRulesStyling}>
          <Link style={fixLink} to={'/home'}><div className="closeButton"><i class="fas fa-times"></i></div></Link>
          <div style={makeBold}><center>GENERAL RULES</center></div>
          <ol>
            <li><p>Tournaments must be formed with at least 4 players . Maximum number of players of each tournament will depend on the duration time selected by the Organizer.</p></li>
            <li><p>Tournament organizer has 7 days to gather players within his/her chosen city.</p></li>
            <li><p>Tournaments may be joined by players within + or – 1.5 level stated on the tournament cards.</p></li>
            <li><p>Each assigned match is given 7 days to take place. A winner must be generated.</p></li>
            <li><p>All matches must be played and logged by either one of the players within the 7 days period.</p></li>
            <li><p>If only one player logged the match within the allotted 7 days period, it means the other player who fails to submit a score automatically agrees with the result.</p></li>
            <li><p>Should there be discrepancies regarding the final score of a match, players should contact the administrator with evidence. Match Point reserves the right in making the final decision of the dispute.</p></li>
            <li><p>Each player has one chance to re-adjust his or her level within 7 days after the first match and within of first week of each season.</p></li>
            <li><p>In the case of an unexpected absence of an opponent. Players should notify the administrators by reporting on the platform. The absent player will be automatically disqualified and receive a warning.</p></li>
            <li><p>Upon receiving a warning from the administrator, the player will be immediately suspended from all upcoming matches for 30 days.</p></li>
          </ol>
        </div>
      </div>
    );
  }
}
export default GeneralRules;
