import React, {Component} from 'react';
import './Profile.css';

class SeasonDots extends Component{
  constructor(props){
    super(props);
    this.pointFillClass=["","","","","","","","","",""];
    for(let i=0;i<this.props.progress;i++){

      this.pointFillClass[i]="filledPoint";
    }
      this.state = {

    }
  }

render() {
  return (

  <div className="seasonPointChart" >
    <div className={this.pointFillClass[0]}></div>
    <div className={this.pointFillClass[1]}></div>
    <div className={this.pointFillClass[2]}></div>
    <div className={this.pointFillClass[3]}></div>
    <div className={this.pointFillClass[4]}></div>
    <div className={this.pointFillClass[5]}></div>
    <div className={this.pointFillClass[6]}></div>
    <div className={this.pointFillClass[7]}></div>
    <div className={this.pointFillClass[8]}></div>
    <div className={this.pointFillClass[9]}></div>
  </div>

);
}
}

export default SeasonDots;
