import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Profile.css';
import ReactTouchEvents from "react-touch-events";


class UserOptions extends Component{
  constructor(props){
    super(props);
    this.handleSwipe=this.handleSwipe.bind(this);
      this.state = {
      }

  }
  handleSwipe (direction) {
      switch (direction) {
        case "top":
            console.log("nothing yet");
            break;
        case "bottom":
            console.log("nothing yet");
            break;
        case "left":
            this.props.swipeClose();
            break;
        case "right":
            this.props.swipeClose();
            console.log("asdasdadsad");
            break;
        default:
            break;
      }
    }

  render() {
    const animatedClass="notAnimated";

    const contStyle={

      height: "86%",
      width: "100%",
      position: "fixed",
      top: "6%",
      right: "0",
      backgroundImage: "linear-gradient(white 70%,#c5ff00)",
      backgroundPosition: "center",
      backgroundSize: "cover",
      zIndex: "2"

    }
    const optionsStyle={
      marginTop: "2rem",
      marginLeft: "1.4rem",
      textAlign: "left",
      fontSize: "1.3em",
      textDecoration: "none",
      color: "grey",
    }
    const fixLink={
      textDecoration: "none",
      color: "grey"
    }
    const pStyle={
      display: "inline",
    }
    const fatLine={
      width: "90%",
      margin: "2rem auto 0 auto",
      borderBottom: "0.05em solid grey"
    }
    return (
      <ReactTouchEvents onSwipe={this.handleSwipe}>
      <div onSwipe={this.handleSwipe} className={animatedClass} style={contStyle}>
        <Link style={fixLink} to="../createTournament"><div style={optionsStyle}><p style={pStyle}>Create Tournament</p></div></Link>
        <div style={fatLine}/>
        {/*<div onClick={this.props.editProfile} style={optionsStyle}><p style={pStyle}>Edit Profile</p></div>*/}
        <Link style={fixLink} to="/home/editProfile"><div style={optionsStyle}><p style={pStyle}>Edit Profile</p></div></Link> {/**/}
        <Link style={fixLink} to="../GeneralRules"><div style={optionsStyle}><p style={pStyle}>General Rules</p></div></Link>
        <div style={optionsStyle}><a style={fixLink} href="mailto:info@matchpoint.pro">Contact Us</a></div>
        <div onClick={this.props.logOut} style={optionsStyle}><p style={pStyle}>Log Out</p></div>
      </div>
    </ReactTouchEvents>
    );
  }
}

export default UserOptions;
