import React, {Component} from 'react';
import './MatchItem.css'
import ParseScore from './ParseScore';
import Defs from '../Defs.js';
import {Link} from 'react-router-dom';


// rgba(9,217,254) BLUE
// rgba(255,207,59) orange

class MatchItem extends Component{
  constructor(props){
    super(props);
    console.log(this.props)
    this.state = {
      isRated: false,
      reviewState: [{color: "black",icon: ""},{color: "rgba(255,127,155)"},{color: "rgba(142,189,177)"},{color: "rgba(100,100,100,0.0)"}],
    }
  }

  render() {
    const itemStyle={
      width: "70%",
      textAlign: "center",
      backgroundColor: "#f7f7f7",
      boxShadow: "0.8em 0.8em 0.7em rgba(0, 0, 0, 0.13)",
      display: "inline-block",
      marginLeft: "6%",
      marginRight: "6%",
      fontSize: "1rem",
      position: "relative",
      wordBreak: "break-all"
    };
    const logButton={
      position: "absolute",
      bottom: "0",
      left: "80%",
      height: "0",
      paddingTop: "20%",
      width: "20%",
      fontSize: "1.5rem",
      backgroundImage: "linear-gradient(135deg, transparent 50%, "+this.state.reviewState[this.props.logState].color+" 50%)"
    };
    const textStyle={
      padding: "0",
      overflow: 'hidden',
      margin: '0 5% 0 5%',
    };
    const partnerName={
      fontSize: "1.2rem",
      margin: "0.2rem 0 0.2rem 0",
      textDecoration: "underline",
      fontWeight: "800",
      fontStyle: "italic",
      color: 'black'
    };
    const topText={
      margin: "6% 0 0 0",
      underline: 'none'
    };
    const triaText={
      height: "1rem",
      position: "absolute",
      fontSize: "0.7rem",
      bottom: "1rem",
      left: "50%",
      color: "white",
      margin: "0",
      padding: "0"
    };

    return (
      <div style={itemStyle}>
        <Link to={'/home/TournamentView/'+this.props.id}><p style={topText}><span style={{textDecoration: "underline", fontWeight: "bold",color: 'black'}}>{this.props.t_name}</span></p></Link>
        <p style={textStyle}>{this.props.t_start} - {this.props.t_end}</p>
        <Link to={'/home/Profile/'+this.props.o_id}><p style={partnerName}>{this.props.o_name}</p></Link>
        {/* <p style={{textStyle, partnerName}}>{this.props.o_name}</p> */}
        {/*this.props.logState > 0 && <p style={{textStyle, partnerName}}><ParseScore score={this.props.score} color="orange"/></p>*/}
        <p style={textStyle}>{this.props.score || 'PLEASE ADD SCORE'}</p>
        <p style={textStyle}>{Defs.cities[this.props.t_location]}</p>
        <div onClick={()=> this.props.logMatch(this.props)} style={logButton}>
          <div style={triaText}>
            <p>{this.props.logState === 0 && "LOG"}{this.props.logState === 1 && <i className="fas fa-question"/>}{this.props.logState === 2 && <i className="fas fa-exclamation"/>}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default MatchItem;
