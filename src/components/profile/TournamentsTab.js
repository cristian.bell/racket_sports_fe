import React, {Component} from 'react';
import MatchItem from './MatchItem';
import './TournamentsTab.css'
import ReviewComp from '../reviewTab/ReviewTab.js';
import ConfirmComp from '../reviewTab/ConfirmComp.js';
import axios from 'axios';
import Defs from '../Defs.js';
import {Link} from 'react-router-dom';


class TournamentsTab extends Component{
  constructor(props){
    super(props);
    this.containerRef=React.createRef();
    this.logMatch=this.logMatch.bind(this);
    this.closeReview=this.closeReview.bind(this);
    this.submitReview=this.submitReview.bind(this);
    this.state = {
      matchList: [{partnerName:"M. CHAN", dateRange:"27.NOV - 10.DEC",gameName: "DEUS OPEN",location: "BERLIN",logState: 0,score: [{you: 6, opponent: 2},{you: 5, opponent: 8},{you: 5, opponent: 8}]},{partnerName:"K. IMAMI",dateRange:"27.JAN - 10.FEB",gameName: "A-PLUS SPORTS OPEN",location: "BERLIN" ,logState: 2,score: [{you: 6, opponent: 2}]},{partnerName:"K. LEE",dateRange:"13.JUN - 2.JUL",gameName: "INTREPID SPORTS OPEN",location: "HONG KONG", logState: 1,score: [{you: 5, opponent: 8},{you: 5, opponent: 8}]}],
      reviewWindow: false,
      confirmWindow: false,
      matchData: [],
      reviewMatch: {partnerName:"K. ASIAN",dateRange:"27.NOV - 10.DEC",gameName: "DEUS OPEN",location: "BERLIN",logState: 0,score: [{you: 5, opponent: 8}]}
    }
  }
  sortByPrio=(arr)=>{
    let firstPrio=[];
    let secondPrio=[];
    let thirdPrio=[];
    arr.forEach(element => {
      if(element.score && element.score_added_by===this.props.uId){
        thirdPrio.push(element)
      }else if(element.score){
        firstPrio.push(element)
      }else{
        secondPrio.push(element)
      }
    });
    return firstPrio.concat(secondPrio.concat(thirdPrio))
  }
  componentDidMount(){
    /* this.containerRef.current.scroll(this.state.matchList.length * 86,0); */
    axios.get(Defs.be_url + "match/mine/",
    {withCredentials:true})
    .then(response =>{
      const data=response.data;
      const sortedData=this.sortByPrio(data);
      this.setState({
        matchData: sortedData
      });
    })
    .catch(response =>{
      console.log(response.status)
    });
  }
  logMatch(matches){
    if (matches.logState === 0) {
      this.setState({
        reviewMatch: matches,
        reviewWindow: true
      });
    } else if(matches.logState === 1) {
     /*  this.setState({
        reviewMatch: matches,
        reviewWindow: true
      }); */
    }
    else if(matches.logState === 2) {
      this.setState({
        reviewMatch: matches,
        confirmWindow: true
      });
    }

  }

  closeReview(){
    this.setState({
      reviewWindow: false,
      confirmWindow:false,
    })
  }
  submitReview(event,data){
    event.preventDefault();
    this.closeReview();
  }

  render() {
    const data=this.state.matchData;
    const matchList=data.map((matches, index) =>{
      return(<MatchItem index={index} logMatch={this.logMatch} logState={matches.score?matches.score_added_by===this.props.uId? 1 : 2 : 0} {...matches}/>)
    });
    return (
      <div ref={this.containerRef} className="sideScroller">
      <div id="reviewContainer">
        {this.state.reviewWindow && <ReviewComp handleSnack={this.props.handleSnack} submitReview={this.submitReview} close={this.closeReview} match={this.state.reviewMatch}/>}
        {this.state.confirmWindow && <ConfirmComp handleSnack={this.props.handleSnack} submitReview={this.submitReview} close={this.closeReview} match={this.state.reviewMatch}/>}
      </div>
      {matchList}
      {data.length<1 && <div id='tournamentJoin'>
        <Link to='/home/TournamentFeed'>JOIN A TOURNAMENT</Link>
        </div>}
      </div>
    );
  }
}

export default TournamentsTab;
