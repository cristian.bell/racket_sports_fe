import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';

class NoteItem extends Component{
  constructor(props){
    super(props);
      this.state = {
        redirectToNote: false,
        expandNote: false,
        expandedNoteHeight: 4
    }

  }
  goToNote=()=>{
    console.log(this.props.note)
  }
  toggleExpand=()=>{
    this.setState({
      expandedNoteHeight: this.state.expandNote? 4:10,
      expandNote: !this.state.expandNote
    })
    this.props.removeNote(this.props.index)
  }

  render() {
    if(this.state.redirectToNote) return(<Redirect to={{
      pathname: '/editProfile',
      state: {reference:this.state}
  }} />);
    const animatedClass="notAnimated";

    const noteStyle={
      marginTop: "1%",
      width: "100%",
      backgroundColor: "rgba(100,100,100,0.2)",
      display: "flex",
      alignItems: "center",
    };
    const textStyle={
      textDecoration: "none",
      fontWeight: 'bold',
      marginLeft: '1rem'
    };
    const fixLink={
      textDecoration: "none",
      marginLeft: "5%",
      color: "grey"
    };

    return (
      <div style={noteStyle}>
      {!this.state.expandNote&&<p onClick={this.toggleExpand} style={textStyle}>{this.props.note.title}</p>}
        {this.state.expandNote&&<div style={{marginLeft: '1rem'}}>
          <p onClick={this.toggleExpand} style={{fontWeight: 'bold',fontSize: '1.2rem'}}>{this.props.note.title}</p>
          <p>{this.props.note.message}</p>
          <Link style={fixLink} to={"/"+this.props.note.redirect}><div style={{fontWeight: 'bold',textDecoration: 'none'}}>GO!</div></Link>
        </div>}
        {/*<Link style={fixLink} to={this.props.note.redirect}><p style={textStyle}>{this.props.note.title}</p></Link>*/}
      </div>
    );
  }
}

export default NoteItem;
