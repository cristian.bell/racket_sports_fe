import React, {Component} from 'react';
import './statsView.css';
import RadarChart from 'react-radar-chart';
import SmallStars from './SmallStars.js';
import MatchHistory from '../../Tournaments/MatchHistory.js';



class StatsView extends Component{
  constructor(props){
    super(props);
    this.setAll=this.setAll.bind(this);
    this.setMy=this.setMy.bind(this);
    this.toggleOptions=this.toggleOptions.bind(this);
      this.state = {
        player: {power: 4,strategy:3,consistency: 2,manner: 3,punctuality:5},
        overview: true,
        tabAll: "noUnderline",
        tabMy: "hasUnderline",
        tabOpen: "my",

    }
  }
  componentWillUpdate(){
    if(this.props.userObject.notificationToggle || this.props.userObject.optionToggle){
        this.props.closeOptions();
    }
}
  setAll(){
    this.setState({
      tabAll: "noUnderline",
      tabMy: "hasUnderline",
      tabOpen: "my"
    })
  }
  setMy(){
    this.setState({
      tabMy: "noUnderline",
      tabAll: "hasUnderline",
      tabOpen: "all"
    })
  }
  toggleOptions(){
    this.setState({
      toggleOptions: !this.state.toggleOptions,

    })
  }
  componentDidMount(){
    this.props.navIndex(0);
  }

  render() {
    const myGroups =
    {
  YOU: {
      color: 'rgba(255,10,10,1)',
      ratings: {
        POWER: this.state.player.power,
        STRATEGY: this.state.player.strategy,
        CONSISTENCY: this.state.player.consistency,
        MANNER: this.state.player.manner,
        PUNCTUALITY: this.state.player.punctuality
      }
  },
  THEM: {
      color: 'rgba(100,100,100,0.6)',
      ratings: {
        POWER: 5,
        STRATEGY: 5,
        CONSISTENCY: 5,
        MANNER: 5,
        PUNCTUALITY: 5
      }
  }


}
    const labels=["POWER",'STRATEGY','CONSISTENCY','MANNER','PUNCTUALITY']


    return (
      <div id='statsCont'>
        <div id="navRow">
          <div onClick={this.setAll} id="allTour" className={this.state.tabAll}>{/*<div onClick={this.toggleOptions} id="searchOptionDiv"><i id="settingsIcon" className="fas fa-sliders-h"></i></div>*/}<p>REVIEWS</p></div>
          <div onClick={this.setMy} id="myTour" className={this.state.tabMy}><p>PAST GAMES</p></div>
        </div>
        <div id='feedContent'>
        {this.state.tabOpen==='all' && <div className='scrollDiv'><MatchHistory></MatchHistory></div>}
        {this.state.tabOpen==='my' &&<div>
        <div id='radar'><RadarChart scaleAlign='top-left' axis={'blue'} axisNames={labels} rungs={5} groups={myGroups}></RadarChart></div>
        <div id='reviewPart'>
          <p>8 REVIEWS{this.props.reviewNum}</p>
          <div className='reviewHeading'>TECHNIQUE</div>
          <div id='myStars'><SmallStars size={0.9} review={(this.state.player.power+this.state.player.strategy+this.state.player.consistency)/3}></SmallStars></div>
          <div className='myReviewCont'>
            <div className='nestedLine'>POWER<div id='mySmallStars'><SmallStars size={0.9} review={this.state.player.power}></SmallStars></div></div>
            <div className='nestedLine'>STRATEGY<div id='mySmallStars'><SmallStars size={0.9} review={this.state.player.strategy}></SmallStars></div></div>
            <div className='nestedLine'>CONSISTENCY<div id='mySmallStars'><SmallStars size={0.9} review={this.state.player.consistency}></SmallStars></div></div>
          </div>
          <div className='reviewHeading'>SPORTSMANSHIP</div>
          <div id='myStars'><SmallStars size={0.9} review={(this.state.player.manner+this.state.player.punctuality)/2}></SmallStars></div>
            <div className='myReviewCont'>
              <div className='nestedLine'>MANNER<div id='mySmallStars'><SmallStars size={0.9} review={this.state.player.manner}></SmallStars></div></div>
              <div className='nestedLine'>PUNCTUALITY<div id='mySmallStars'><SmallStars size={0.9} review={this.state.player.punctuality}></SmallStars></div></div>
            </div>
        </div>
      </div>}
      </div>
      </div>
    );
  }
}

export default StatsView;
