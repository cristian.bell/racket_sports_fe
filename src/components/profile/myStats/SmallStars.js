import React, {Component} from 'react';


class SmallStars extends Component{
  constructor(props){
    super(props);

    this.state = {

    }
  }
  componentDidMount(){
  }


  render() {

    const rateStars={

      width: "100%",
      margin: "0 auto",
      display: "flex",
      justifyContent: "space-around",
      alignItems: "center"
    }
    const reviewStar={
      height: "1.2rem",
      width: "1.2rem",
      display: "flex",
      alignItems: "center"
    }
    const star={
      margin: "0 auto",
      fontSize: this.props.size+"rem"
    }

    return(

        <div style={rateStars}>
          <div  style={reviewStar}>{this.props.review>0 && <i style={star} className="fas fa-star"></i>}{this.props.review<=0 && <i style={star} className="far fa-star"></i>}</div>
          <div  style={reviewStar}>{this.props.review>1 && <i style={star} className="fas fa-star"></i>}{this.props.review<=1 && <i style={star} className="far fa-star"></i>}</div>
          <div  style={reviewStar}>{this.props.review>2 && <i style={star} className="fas fa-star"></i>}{this.props.review<=2 && <i style={star} className="far fa-star"></i>}</div>
          <div  style={reviewStar}>{this.props.review>3 && <i style={star} className="fas fa-star"></i>}{this.props.review<=3 && <i style={star} className="far fa-star"></i>}</div>
          <div  style={reviewStar}>{this.props.review>4 && <i style={star} className="fas fa-star"></i>}{this.props.review<=4 && <i style={star} className="far fa-star"></i>}</div>
        </div>

    );
  }
}

export default SmallStars;
