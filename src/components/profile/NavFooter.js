import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './animateNav.css';

export class NavFooter extends Component {

  constructor(props) {
    super(props);
    console.log(this.props);
    this.clickNav=["","","",""];
    this.navClicked=this.navClicked.bind(this);
    this.resetAnimation=this.resetAnimation.bind(this);
    this.state = {
       clickNav: ["","","",""]
    }
  }

  resetAnimation=function(i){

  };

  navClicked(i){

    this.clickNav=["","","",""];
    this.clickNav[i]="clicked";

    this.setState({
      clickNav: this.clickNav
    })

  }

  render() {
    return (
      <footer className="navFoot">
       <Link onClick={()=>this.navClicked(0)} to={'/home/StatsView'}><div id={this.props.navIndex === 0 ?  'clicked' : ''} className="stats"/></Link>
       <Link onClick={()=>this.navClicked(1)} to={'/home'}><div id={this.props.navIndex === 1 ? 'clicked' : ''} className="home"/></Link>
       <Link onClick={()=>this.navClicked(2)} to={'/home/TournamentFeed'}><div id={this.props.navIndex === 2 ? 'clicked' : ''} className="games"/></Link>
       <Link onClick={()=>this.navClicked(3)} to={'/home/Store'}><div id={this.props.navIndex === 3 ? 'clicked' : ''} className="achievements"/></Link>
      </footer>
    )
  }
}

export default NavFooter




