import React, {Component} from 'react';
import './Profile.css';



class ProfileSearch extends Component{
  constructor(props){
    super(props);
      this.state = {
        mySearch: ""
    }
  }
  handleChange(event) {
     this.setState({mySearch: event.target.value});
   }
  handleSearch(event){
    event.preventDefault();
    console.log("your search was: "+ this.state.mySearch)
  }


  render() {
    return (
        <form className="searchBar search">
        <button type="Submit" onClick={this.handleSearch.bind(this)} className="searchIt search"><i class="fas fa-search"/></button>
        <input onChange={this.handleChange.bind(this)} type="search" id="playerSearch" class="search" name="q" placeholder="SEARCH FOR PLAYERS..."/>
        <button onClick={this.props.myOptions} className="searchSettings search"><i class="fas fa-sliders-h"/></button>
      </form>
    );
  }
}

export default ProfileSearch;
