import React, {Component} from 'react';
import './Profile.css';


class ProfileSummary extends Component{
  constructor(props){
    super(props);
      this.state = {

    }
  }

  render() {
    return (
      <section className="profSec">
        <div className="profLine firstLine">
          <div className="lvlProf">
            <p>{this.props.playerSkill}</p>
          </div>
          <p className="mainProf">{this.props.playerSkill}</p>
        </div>
        <div className="profLine">
          <div className="mainHandProf">
            <p>{this.props.dominantHand.short}</p>
          </div>
          <p className="mainProf">{this.props.dominantHand.desc}</p>
        </div>
        <div className="profLine">
          <div className="backHandProf">
            <p>{this.props.backHand.short}</p>
          </div>
          <p className="mainProf">{this.props.backHand.desc}</p>
        </div>
        <div className="profLine">
          <div className="spinProf"><p>{this.props.mainSkill.short}</p></div>
          <p className="mainProf">{this.props.mainSkill.desc}</p>
        </div>
        <div className="profLine">
          <div className="ageProf">
            <p>A</p>
          </div>
          <p className="mainProf">{this.props.experience} YEARS</p>
        </div>
        <div className="profLine">
          <div className="frequProf">
            <p>F</p>
          </div>
          <p className="mainProf"> {this.props.playFrequence}</p>
        </div>
      </section>
    );
  }
}

export default ProfileSummary;
