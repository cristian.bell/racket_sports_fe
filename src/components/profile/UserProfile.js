import React, {Component} from 'react';
import { Redirect} from 'react-router-dom';
import PieChart from 'react-minimal-pie-chart';
import axios from 'axios';
import ProfileImage from './ProfileImage.js';
import './Profile.css';
import './UserProfile.css';
import TournamentsTab from './TournamentsTab';
import SeasonDots from './SeasonDots';
import QuickDesc from './QuickDesc.js';
import StoreTab from './StoreTab.js';
import Defs from '../Defs.js';
import myPic from '../../images/userIcon.png';


class UserProfile extends Component {
  constructor(props) {
    super(props);
      //console.log(this.props)
    this.state = {
      uId: this.props.uId,
      name : "KEN LEE",
      location: "BERLIN",
      profilePic: myPic,
      genderOption: Defs.genders,
      gender: "",
      age: "",
      email: "",
      phone: "",
      estuff: "UPON REQUEST", //show email? can be just boolean
      mstuff: "UPON REQUEST", //show mobile? can be just bolean
      currentGear: "BABOLAI PURE DRIVE (2014 VER.)",
      gamesPlayed : 14,
      gamesWon : 13,
      dominantHandOption: [{desc: "RIGHT", short: "R"},{desc: "RIGHT", short: "R"},{desc: "LEFT", short: "L"}],
      dominantHand: {desc: "RIGHT", short: "L"},
      backHandOption: [{desc: "SINGLE", short: "S"},{desc: "SINGLE", short: "S"},{desc: "DOUBLE", short: "DB"}],
      backHand: {desc: "SINGLE", short: "S"},
      playFrequenceOption: ["NON FREQUENT","NON FREQUENT","FREQUENT PLAYER","VERY FREQUENT PLAYER","SERIOUS PLAYER"],
      playFrequence: "",
      experience : 6,
      playerSkill: "6.0",

      mainSkillOption:[{desc: "STRONG FOREHAND",short: "SF",num: 0},{desc: "STRONG FOREHAND",short: "SF",num: 0},{desc: "STRONG BACKHAND",short: "SF",num: 1},{desc: "BASELINER",short: "BL",num: 2},{desc: "SERVE",short: "SE",num: 3},{desc: "TACTICAL PLAYER",short: "TP",num: 4},{desc: "ALL ROUND",short: "AR",num: 5}],  // object {skill: 1-6, imageOfSkill: url}
      mainSkill: {desc: "STRONG FOREHAND",short: "SF",num: 0},

      character: 5,
      technique: 3,

      lastGameDate: "20.4.1889",

      seasonProgress: [5,3,4,9], // progress of season achievements 1-10 maybe people could unlock new achievements after filled up
      matchList: [{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spanda"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spanda"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"}],

      userData: {},
      logOutMessage: "YOU HAVE BEEN LOGGED OUT!",
      loggedOut: false,
          photo: null
    }
  }

componentDidMount(){
    this.props.navIndex(1);
    axios.get(Defs.be_url + "user/" + this.props.uId)
    .then(response =>{
        const userData = response.data;
        this.setState({userData});
        this.setState({
            name: this.state.userData.firstname + " " + this.state.userData.lastname,
            location: this.state.userData.city, /*+ " " + this.state.userData.country,*/
            playerSkill: this.state.userData.level/10,
            //dominantHand: this.state.dominantHandOption[this.state.userData.dominant_hand],
            //backHand: this.state.backHandOption[this.state.userData.dominant_hand],
            experience: this.state.userData.play_years,
            playFrequence: this.state.playFrequenceOption[this.state.userData.play_times],
            gamesPlayed: this.state.userData.gamesPlayed,
            gamesWon: this.state.userData.gamesWon,
            //mainSkill: this.state.mainSkillOption[this.state.userData.main_skill],
            lastGameDate: this.state.userData.lastGameDate,
            //currentGear: this.state.userData.gear_1,
            gender: this.state.genderOption[this.state.userData.gender],
            age: this.state.userData.age,
            email: this.state.userData.email,
            phone: this.state.userData.phone,
            profilePic: response.data.photo
          })
        })
        .catch(response =>{
          console.log(response.status)
        });
  }

  //UserProfile should be managed by UserSite and not grab its own state

  render() {
    if(this.state.loggedOut)return(<Redirect to={{
              pathname: '/LogIn',
              state: { reference: this.state.logOutMessage }
          }} />);
    return (
      <div style={{height: window.innerHeight*1.5+"px"}} className="profileCompUser">

      <div className="userProfileContainer">
      <section id="sectionOne">
      <div id="mainInfo">
        <div id="profileImageContainerUser"><ProfileImage source={this.state.profilePic}/></div>
          <div id="userProfileDesc" className="characterDesc">
            <p className="profileName"><span style={{fontWeight: "bold"}}>{this.state.name}</span> <br/>{this.state.location}</p>
          </div>
          <div id="quickDesc"><QuickDesc
              playerSkill={this.state.playerSkill}
              dominantHand={this.state.dominantHand.short}
              backHand={this.state.backHand.short}
              mainSkill={this.state.mainSkill.num}/></div>
      </div>

      <div id="userChartDiv" className="matchesPlayed">
          <div id="userChartP"><div id="userChartPlayed"><p>MATCH PLAYED: <span style={{fontWeight: "bold",fontSize: "1rem"}}>{this.state.gamesPlayed}</span></p></div>
          <div id="userChartWR"><p>W: <span style={{fontWeight: "bold",fontSize: "1rem"}}>{this.state.gamesWon}</span> L: <span style={{fontWeight: "bold",fontSize: "1rem"}}>{this.state.gamesPlayed-this.state.gamesWon}</span></p></div></div>
          <div id="WrHeader"/>
          <div id="userWinrateChart" className="winRateChart">
            <div id="pieInfo"><p>W: {Math.round((this.state.gamesWon / this.state.gamesPlayed) * 100)} %</p></div>
        <PieChart startAngle={270}
          data={[
            { value: this.state.gamesWon, color: "rgba(197,255,0)" },
            { value: this.state.gamesPlayed - this.state.gamesWon, color: "rgba(134,230,62)", opacity: "1.0" }
            ,
          ]}/>
        </div>
        </div>
      </section>

      <section id="mainSection">
        <div id="tournamentIntro">
          <div className="userSectionCaption">TOURNAMENTS</div>
          <p id="tournamentP">3 MATCHES TO BE PLAYED</p>
        </div>
        <div id="tournamentTab">
          <TournamentsTab handleSnack={this.props.handleSnack} uId={this.state.uId} matchList={this.state.matchList}/>
        </div>
        <div id="seasonIntro">
          <div className="userSectionCaption">SEASONAL GOALS</div>

        </div>
        <div  id="seasonTab">
          <div><div className="seasonDescP"><p className="boldP">WIN <span>3</span></p><p>TOURNAMENTS ({this.state.tournamentsWon}/3)</p></div><SeasonDots progress={this.state.seasonProgress[0]}/></div>
          <div><div className="seasonDescP"><p className="boldP">WIN <span>10</span></p><p>MATCHES ({this.state.gamesWon}/10)</p></div><SeasonDots progress={this.state.seasonProgress[1]}/></div>
          <div><div className="seasonDescP"><p className="boldP">PLAY <span>50</span></p><p>MATCHES ({this.state.gamesPlayed}/50)</p></div><SeasonDots progress={this.state.seasonProgress[2]}/></div>
          <div><div className="seasonDescP"><p className="boldP">PLAY <span>20</span></p><p>NEW PLAYERS ({this.state.newPlayers}/20)</p></div><SeasonDots progress={this.state.seasonProgress[3]}/></div>
        </div>
        <div id="storeIntro">
          <div className="userSectionCaption">HITCOIN STORE</div>
        </div>

        <div id="storeTab">
          <StoreTab/>
        </div>
        </section>
      </div>

      </div>
    );
  }
}

export default UserProfile;
