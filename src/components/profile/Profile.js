import React, {Component} from 'react';
import axios from 'axios';
import PieChart from 'react-minimal-pie-chart';
import ProfileImage from './ProfileImage.js';
import SkillDescDots from './SkillDescDots';
import RequestMatch from '../RequestMatch/RequestMatch.js';
import profileImage from './profileTestImage.jpg';
import RadarChart from 'react-radar-chart';
import QuickDesc from './QuickDesc.js';
import Defs from '../Defs.js';
import './Profile.css';


class Profile extends Component{
  constructor(props){
    super(props);
    /* TODO make call with the id passed through router */
    if(this.props.match.params.id){
      console.log(this.props.match.params.id)
    }
    this.sendRequest=this.sendRequest.bind(this);
    this.closeWindow=this.closeWindow.bind(this);
      this.state = {
        name : "Simona Halep",
        location: "BERLIN",
        profilePic: profileImage,
        genderOption: ["MALE","FEMALE","OTHER","WARTANK","POTATO"],
        gender: "",
        age: "",
        email: "",
        phone: "",
        estuff: "UPON REQUEST", //show email? can be just boolean
        mstuff: "UPON REQUEST", //show mobile? can be just bolean
        currentGear: "BABOLAI PURE DRIVE (2014 VER.)",
        gamesPlayed : 14,
        gamesWon : 9,
        dominantHandOption: [{desc: "RIGHT", short: "R"},{desc: "RIGHT", short: "R"},{desc: "LEFT", short: "L"}],
        dominantHand: {desc: "RIGHT", short: "R"},
        backHandOption: [{desc: "SINGLE", short: "S"},{desc: "SINGLE", short: "S"},{desc: "DOUBLE", short: "DB"}],
        backHand: {desc: "SINGLE", short: "S"},
        playFrequenceOption: ["NON FREQUENT","NON FREQUENT","FREQUENT PLAYER","VERY FREQUENT PLAYER","SERIOUS PLAYER"],
        playFrequence: "",
        experience : 6,
        playerSkill: "6.0",

        mainSkillOption:[{desc: "STRONG FOREHAND",short: "SF"},{desc: "STRONG FOREHAND",short: "SF"},{desc: "STRONG BACKHAND",short: "SF"},{desc: "BASELINER",short: "BL"},{desc: "SERVE",short: "SE"},{desc: "TACTICAL PLAYER",short: "TP"},{desc: "ALL ROUND",short: "AR"}],  // object {skill: 1-6, imageOfSkill: url}
        mainSkill: {desc: "STRONG FOREHAND",short: "SF",num: 0},

        character: 5,
        technique: 3,

        lastGameDate: "20.4.1889",

        seasonProgress: [5,3,4,9], // progress of season achievements 1-10 maybe people could unlock new achievements after filled up
        matchList: [{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin Spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin Spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin Spandauu"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin Spandauu"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin Spandauu"}],

        userData: {},
        requestOpen: false,
        player: {power: 4,strategy:3,consistency: 2,manner: 3,punctuality:5},
    }
  }

  componentDidMount(){
    axios.get(Defs.be_url + "user/" + this.props.match.params.id)
    .then(response =>{
      console.log(response.data)
      
        const userData=response.data;
          this.setState({
            name: userData.firstname+" "+userData.lastname,
            location: userData.city+" "+userData.country,
            playerSkill: userData.level/10,
            dominantHand: this.state.dominantHandOption[userData.dominant_hand],
            backHand: this.state.backHandOption[userData.dominant_hand],
            experience: userData.play_years,
            playFrequence: this.state.playFrequenceOption[userData.play_times],
            gamesPlayed: userData.gamesPlayed,
            gamesWon: userData.gamesWon,
            mainSkill: this.state.mainSkillOption[userData.main_skill],
            lastGameDate: this.state.userData.lastGameDate,
            currentGear: this.state.userData.gear_1,
            gender: this.state.genderOption[userData.gender],
            age: userData.age,
            email: userData.email,
            phone: userData.phone
          })
      

        })
        .catch(response =>{
          console.log(response.status)
        });
  }

  sendRequest(event){
    this.setState({
      requestOpen: true
    })
  }
  closeWindow(event){
    this.setState({
      requestOpen: false
    })
  }
  render() {
    const myGroups =
    {
  YOU: {
      color: 'rgba(100,10,255,1)',
      ratings: {
        POWER: this.state.player.power,
        STRATEGY: this.state.player.strategy,
        CONSISTENCY: this.state.player.consistency,
        MANNER: this.state.player.manner,
        PUNCTUALITY: this.state.player.punctuality
      }
  },
  THEM: {
      color: 'rgba(100,100,100,0.6)',
      ratings: {
        POWER: 5,
        STRATEGY: 5,
        CONSISTENCY: 5,
        MANNER: 5,
        PUNCTUALITY: 5
      }
  }
};
    const labels=["POWER",'STRATEGY','CONSISTENCY','MANNER','PUNCTUALITY'];
    return (
      <div  className="profileComp">
      {this.state.requestOpen && <RequestMatch userData={this.props} closeWindow={this.closeWindow} player={this.state} />}
      <div  className="profileContainer">
        <div id='quickIntro'>
        <div className="matchesPlayed">
          <p>MATCH PLAYED: {this.state.gamesPlayed} W: {this.state.gamesWon} L: {this.state.gamesPlayed-this.state.gamesWon}<span>W:{Math.round((this.state.gamesWon/this.state.gamesPlayed)*100)} %</span></p>
          <div id='winRateP' />
        </div>

        <div className="mergedWinChart">

          <div id="mergedImg"><ProfileImage source={this.state.profilePic}/></div>
         <PieChart
            data={[
              { value: this.state.gamesWon, color: "rgba(36,255,225,0.55)" },
              { value: this.state.gamesPlayed-this.state.gamesWon, color: 'rgba(100,100,100,0)' }
              ,
            ]}
          />
        </div>
        </div>
        <div id='quickDescSquare'><QuickDesc playerSkill={this.state.playerSkill} backHand={this.state.backHand.short} dominantHand={this.state.dominantHand.short} mainSkill={this.state.mainSkill.num} /></div>

          <p className="profileName">{this.state.name}, {this.state.location}</p>
          <p className="profileName">{this.state.currentGear}</p>
          {/*<p className="quest">E: {this.state.estuff}</p>
          <p className="quest">M: {this.state.estuff}</p>
          <p className="achieve">{this.state.currentGear}</p>*/}

        {/*<div className="requestButton">
          <button onClick={this.sendRequest} type="button" name="button">REQUEST TO PLAY</button>
        </div>*/}
        <div className="skillDesc">
          <p className="skillDescP">CHARACTER</p>
          <SkillDescDots fillDots={this.state.character} />
          <p className="skillDescP">TECHNIQUE</p>
          <SkillDescDots fillDots={this.state.technique} />
        </div>

        <div id='radarProf'>
            <RadarChart scaleAlign='top-left' axis={'blue'} axisNames={labels} rungs={5} groups={myGroups}/>
        </div>


        </div>

      </div>
    );
  }
}

export default Profile;
