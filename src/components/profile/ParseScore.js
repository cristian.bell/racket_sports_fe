import React, {Component} from 'react';


class ParseScore extends Component{
  constructor(props){
    super(props);

    this.state = {
      isRated: false,
      reviewState: [{color: "black",icon: ""},{color: "orange"},{color: "blue"},{color: "rgba(100,100,100,0.0)"}],
    }
  }

  render() {
    const scoreSize={
      fontSize: 1.4+"rem",
      textDecoration: "none !important",
      color: "grey"
    }

    return (
      <div>
      {this.props.score.map(score=> <span style={scoreSize}><span/>  ({score.you} - {score.opponent})  </span>)}
      </div>
    );
  }
}

export default ParseScore;
