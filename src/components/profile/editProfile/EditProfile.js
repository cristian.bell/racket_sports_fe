import React, {Component} from 'react';
import './EditProfile.css';
import ProfileImage from '../ProfileImage.js';
import EditRow from './EditRow.js';
import BlackButton from '../../Tournaments/BlackButton.js';
import axios from 'axios';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Defs from '../../Defs.js';
import { Redirect} from 'react-router-dom';


class EditProfile extends Component {
    onChange = (crop, pixelCrop) => {
        this.setState({crop, pixelCrop});
        //console.log(pixelCrop);
    }
    // Note: only for modern browser

// helper function: generate a new file from base64 String

    /**
     * @param {File} image - Image File Object
     * @param {Object} pixelCrop - pixelCrop Object provided by react-image-crop
     * @param {String} fileName - Name of the returned file in Promise
     */
    rotateImage = (image) => {
        // rotate image if needed pretty sure people shoud just do on their phone
        const rotateCanvas = document.createElement('canvas');
        const rImage = new Image();
        rImage.src = image;
        const maxDim = rImage.height > rImage.width ? rImage.height : rImage.width;
        rotateCanvas.width = maxDim;
        rotateCanvas.height = maxDim;
        const rCtx = rotateCanvas.getContext('2d');

        rCtx.save();
        rCtx.translate(maxDim / 2, maxDim / 2);
        rCtx.rotate(90 * Math.PI / 180);
        rCtx.drawImage(rImage, -rImage.width / 2, -rImage.height / 2);
        rCtx.restore();
        const rotatedPic = rotateCanvas.toDataURL('image/png');
        console.log(rotatedPic);
        this.setState({uploadedPic: rotatedPic})
    };
    getCroppedImg = (image, pixelCrop) => {
        if (pixelCrop.width > 10 && pixelCrop.height > 10) {
            const canvas = document.createElement('canvas');
            canvas.width = pixelCrop.width;
            canvas.height = pixelCrop.height;
            const ctx = canvas.getContext('2d');
            const imageOne = new Image();
            imageOne.src = image;
            ctx.drawImage(
                imageOne,
                pixelCrop.x,
                pixelCrop.y,
                pixelCrop.width,
                pixelCrop.height,
                0,
                0,
                pixelCrop.width,
                pixelCrop.height
            );

            // As Base64 string
            this.croppedImage = canvas.toDataURL('image/jpeg'); // SEND THIS TO THE SERVER

            /*let newImage=new Image();
            canvas.toBlob((blob)=>{
              this.imgBlob=blob;
              //newImage.src=URL.revokeObjectURL(URL.createObjectURL(blob));
              //console.log(newImage)
            },'image/jpeg',1)

            if (this.imgBlob !== null)
            console.log(this.imgBlob)
                this.sendPicture();
            */
            if (this.croppedImage !== null)
                this.sendPicture();


            this.setState({
                profilePic: this.croppedImage, cropOpen: false, crop: {
                    aspect: 16 / 16,
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                }, pixelCrop: {x: 0, y: 0, width: 0, height: 0}
            })
            // As a blob
        } else {
            this.setState({cropOpen: false})
        }
    };
    uploadPicture = (event) => {
        const imageData = event.target.files;
        let reader = new FileReader();
        let file = imageData[0];
        reader.onloadend = () => {
            this.setState({uploadedPic: reader.result});
            // maybe rather go straight to crop and make people rotate their pictures in phone app.
            //this.setState({viewImage:true}) if people want to preview their stuff and maybe rotate it... kinda useless and dogshit performance
            this.setState({cropOpen: true})
        };
        if (file) {
            reader.readAsDataURL(file);
        }
    }
    sendPicture = (event) => {
        const formData = new FormData();
        formData.append("image", this.croppedImage);

        axios({
            method: 'patch',
            url: Defs.be_url + 'user/photo',
            withCredentials: true,
            /*,headers:{'x-api-key': '123'}, */
            data: formData
        })
            .then(response => {
                console.log(response);
                if (response.status === 200) {
                    console.log(this.state);
                }
            })
            .catch(response => {
                console.log(response);
                if (response.status === 413) {
                    console.log('file too large');
                }
                //this.props.history.goBack();
            });
    }

    constructor(props) {
        super(props);
        this.props.closeOptions();
        //this.userObject = this.props.location.state.reference;
        this.userObject = this.props.userObject;
        console.dir(this.userObject);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.croppedImage = null; // the cropped Image is saved as base64 string can be send to your backend
        this.imgBlob = null;//todo check this
        this.state = {
            phone: "",
            mail: this.userObject.email,
            mailCheck: 1,
            phoneCheck: false,
            pictureData: {},
            mainSkill: this.userObject.mainSkill.short,
            location: this.userObject.location,
            profilePic: this.userObject.profilePic,
            lvlSlider: this.userObject.playerSkill,
            cropOpen: false,
            uploadedPic: 0,
            viewImage: false,
            crop: {
                aspect: 16 / 16,
                x: 0,
                y: 0,
                width: 0,
                height: 0
            },
            pixelCrop: {
                x: 0,
                y: 0,
                width: 0,
                height: 0
            },
            city: null,
            returnToUserSite: false
        }

    }
    componentWillUpdate(){
        if(this.props.userObject.notificationToggle || this.props.userObject.optionToggle){
            this.props.closeOptions();
        }
    }

    componentDidMount() {
        this.setState({
            mail: this.userObject.email,
            mainSkill: this.userObject.mainSkill.short,
            phone: this.userObject.phone,
            profilePic:this.userObject.profilePic,
            lvlSlider: this.userObject.playerSkill,
        })
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const dataOut = {
            "email": this.state.mail,
            "city": this.state.city,
            "level": this.state.lvlSlider
        };

        axios({
            method: 'patch',
            url: Defs.be_url + 'user/save',
            withCredentials: true,
            /*headers:{'x-api-key': '123'}, */
            data: dataOut
        })
        .then(response => {
            console.log(response);
            console.log(response.status);
            if (response.status === 200) {
                this.props.handleSnack("SUCCESFULLY UPDATE PROFILE", 0, false)
            }
        })
        .catch(response => {
            console.log(response);
            //this.props.history.goBack();
            this.props.handleSnack("EDIT NOT SUCCESFUL", 1, false);
            this.setState({returnToUserSite: true})
        });
    }

    render() {
      if (this.state.returnToUserSite) {
        return(<Redirect to={{
          pathname: '/home',
          state: { reference: this.state}
      }} />);
      }
        return (
            <div style={{height: window.innerHeight + "px"}} id="editProfileCont">
                {/*this.state.viewImage&&<div id="previewWindow">
          <div id='previewImage' style={{backgroundImage:"url("+this.state.uploadedPic+")"}}></div>
          <div id='cropSubmit' onClick={()=>this.rotateImage(this.state.uploadedPic)}>ROTATE</div>
          <div id='cropSubmit' onClick={()=>this.setState({viewImage: false,cropOpen: true})}>DONE</div>
    </div>*/}
                {this.state.cropOpen && <div id='cropWindow'>
                    <p id="cropText">MAKE A FANCY SQUARE</p>
                    <ReactCrop src={this.state.uploadedPic} crop={this.state.crop} onChange={this.onChange}/>
                    <div id='cropSubmit'
                         onClick={() => this.getCroppedImg(this.state.uploadedPic, this.state.pixelCrop)}>FINISH CROP
                    </div>
                </div>}
                {!this.state.cropOpen && <div id="editContent">
                    <div id="editPicture">
                        <div id="picDiv">
                            <ProfileImage source={this.state.profilePic}/>
                            <div id="optionsDiv">
                                <input id="changePic" className="fileInput" name="changePic" type="file"
                                       onChange={this.uploadPicture}/>
                                <label id="changePicLabel" htmlFor="changePic"><i className="far fa-edit"/></label>
                            </div>
                        </div>
                    </div>
                    <div className="editRows" id="editContact">
                        <div id="mailRow"><EditRow check={this.state.mailCheck} value={this.state.mail}
                                                   label="YOUR MAIL" type="mail" name="mail" checkBox={false}
                                                   readOnly={true}
                                                   onChange={this.handleInputChange}/></div>
                        <div id="mailRow"><EditRow check={this.state.phoneCheck} value={this.state.phone}
                                                   label="YOUR PHONE" type="number" name="phone" checkBox={false}
                                                   readOnly={false}
                                                   onChange={this.handleInputChange}/></div>
                        <div id="locationDiv" style={{display: 'flex', justifyContent:'space-between'}}>
                            <div style={{width: '40%'}}>LOCATION</div>
                            <div style={{width: '60%'}}>
                                <select
                                    style={{marginTop: 0}}
                                    onChange={this.handleInputChange}
                                    name="location"
                                    className="signInputTwo">{Defs.cities.map((city,i) => <option value={i} key={i}>{city}</option>)}</select>
                            </div>
                        </div>
                    </div>
                    <div id="editMore">
                        <p id="editDescP">PLAY STYLE (EDIT ONCE PER SEASON)</p>
                        <div id="styleDiv">
                            <div id="styleOne">
                                <input value="1" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "1"} name="mainSkill" id="styleCheck1"
                                       type="radio"/>
                                <label id="sForehand" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck1"/>
                                <p>Strong Forehand</p>
                                <input value="2" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "2"} name="mainSkill" id="styleCheck2"
                                       type="radio"/>
                                <label id="sBackhand" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck2"/>
                                <p>Strong Backhand</p>
                                <input value="3" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "3"} name="mainSkill" id="styleCheck3"
                                       type="radio"/>
                                <label id="sBaseliner" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck3"/>
                                <p>Baseliner</p>
                            </div>

                            <div id="styleTwo">
                                <input value="4" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "4"} name="mainSkill" id="styleCheck4"
                                       type="radio"/>
                                <label id="sServer" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck4"/>
                                <p>Serve & Volley</p>
                                <input value="5" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "5"} name="mainSkill" id="styleCheck5"
                                       type="radio"/>
                                <label id="sTactical" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck5"/>
                                <p>Tactical</p>
                                <input value="6" onChange={this.handleInputChange}
                                       checked={this.state.mainSkill === "6"} name="mainSkill" id="styleCheck6"
                                       type="radio"/>
                                <label id="sAllround" name="playStyle" className="checkLabel"
                                       htmlFor="styleCheck6"/>
                                <p>All Round</p>
                            </div>
                        </div>
                    </div>
                    <div id="sliderDiv">
                        <p>{this.state.lvlSlider / 10}</p>
                        <input step="5" onChange={this.handleInputChange} name="lvlSlider" type="range" min="0" max="70"
                               value={this.state.lvlSlider} className="slider"/>
                    </div>
                    <div onClick={this.handleSubmit} id="buttonContainer"><BlackButton content={"SUBMIT"}/>
                    </div>
                </div>}
            </div>
        );
    }
}

export default EditProfile;
