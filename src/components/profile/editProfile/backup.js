import React, {Component} from 'react';
import { Redirect} from 'react-router-dom';
import './EditProfile.css';
import ProfileImage from '../ProfileImage.js';
import EditRow from './EditRow.js';
import BlackButton from '../../Tournaments/BlackButton.js';
import axios from 'axios';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';



class EditProfile extends Component{
  constructor(props){
    super(props);
    this.userObject=this.props.location.state.reference
    this.handleInputChange=this.handleInputChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
    this.croppedImage=''; // the cropped Image is saved as base64 string can be send to your backend
      this.state = {
        phone: "016969696969",
        mail: "helloworld@mail.com",
        mailCheck: 1,
        phoneCheck: false,
        pictureData: {},
        mainSkill: "1",
        location: this.userObject.location,
        profilePic: this.userObject.profilePic,
        lvlSlider: this.userObject.playerSkill*10,
        cropOpen: false,
        uploadedPic: 0,
        viewImage: false,
        crop: {
          aspect: 16/16,
          x: 0,
          y: 0,
          width:0,
          height:0
         },
         pixelCrop: {
           x:0,
           y:0,
           width: 0,
           height: 0
         },
         returnToUserSite: false
    }

  }
  componentDidMount(){
    this.setState({
      mail: this.userObject.email,
      mainSkill: this.userObject.mainSkill,
      phone: this.userObject.phone
    })
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

  }
  handleSubmit(event){
    event.preventDefault();
    const formData=new FormData();
    formData.append("email", this.state.mail);
    formData.append("city", this.state.city);
    formData.append("level",this.state.lvlSlider/10);
    

    axios({ 
      method: 'patch', 
      url: 'https://bend.cristianbell.com/user/save'
      /*,headers:{'x-api-key': '123'}*/,
        data: formData})
    .then(response =>{
        console.log(response);
        console.log(response.status);
        console.log("at least done it");
        if(response.status === 200){
          
        }
        })
        .catch(response =>{
          console.log(response);
          //this.props.history.goBack();
          this.setState({returnToUserSite: true})
        });
    

  }
  onChange = (crop, pixelCrop) => {
    this.setState({ crop,pixelCrop });
    console.log(pixelCrop)

  }

  /**
 * @param {File} image - Image File Object
 * @param {Object} pixelCrop - pixelCrop Object provided by react-image-crop
 * @param {String} fileName - Name of the returned file in Promise
 */
rotateImage=(image)=>{
  // rotate image if needed pretty sure people shoud just do on their phone 
  const rotateCanvas=document.createElement('canvas');
  const rImage=new Image();
  rImage.src=image;
  const maxDim=rImage.height>rImage.width? rImage.height : rImage.width;
  rotateCanvas.width=maxDim;
  rotateCanvas.height=maxDim;
  const rCtx=rotateCanvas.getContext('2d');

  rCtx.save();
  rCtx.translate(maxDim/2,maxDim/2);
  rCtx.rotate(90*Math.PI/180);
  rCtx.drawImage(rImage,-rImage.width/2,-rImage.height/2)
  rCtx.restore();
  const rotatedPic=rotateCanvas.toDataURL('image/png');
  console.log(rotatedPic)
  this.setState({uploadedPic:rotatedPic})
}
getCroppedImg=(image, pixelCrop)=> {
  if(pixelCrop.width>10 && pixelCrop.height>10){
  const canvas = document.createElement('canvas');
  canvas.width = pixelCrop.width;
  canvas.height = pixelCrop.height;
  const ctx = canvas.getContext('2d');
  const imageOne=new Image();
  imageOne.src=image;
  ctx.drawImage(
    imageOne,
    pixelCrop.x,
    pixelCrop.y,
    pixelCrop.width,
    pixelCrop.height,
    0,
    0,
    pixelCrop.width,
    pixelCrop.height
  );

  // As Base64 string
  this.croppedImage = canvas.toDataURL('image/jpeg'); // SEND THIS TO THE SERVER
  canvas.toBlob((blob)=>{
    console.log(blob)
  },'image/jpeg',1)
  console.log(this.croppedImage)
  this.setState({profilePic:this.croppedImage,cropOpen: false,crop: {
    aspect: 16/16,
    x: 0,
    y: 0,
    width:0,
    height:0
   },pixelCrop:{x:0,y:0,width:0,height:0}})
  // As a blob
  }else{
    this.setState({cropOpen: false})  
  } 
}
uploadPicture=(event)=>{
  const imageData  =event.target.files; 
  let reader=new FileReader();
  let file=imageData[0];
  reader.onloadend=()=>{
    this.setState({uploadedPic:reader.result});
    // maybe rather go straight to crop and make people rotate their pictures in phone app.
    //this.setState({viewImage:true}) if people want to preview their stuff and maybe rotate it... kinda useless and dogshit performance
    this.setState({cropOpen: true})
  }
  if(file){
    reader.readAsDataURL(file);
  }
  
}


  render() {
    if(this.state.returnToUserSite){
      return(<Redirect to={{
        pathname: '/home',
        state: { reference:this.state}
    }} />);
    }
    return (
      <div style={{height: window.innerHeight +"px"}} id="editProfileCont">
        {/*this.state.viewImage&&<div id="previewWindow">
          <div id='previewImage' style={{backgroundImage:"url("+this.state.uploadedPic+")"}}></div>
          <div id='cropSubmit' onClick={()=>this.rotateImage(this.state.uploadedPic)}>ROTATE</div>
          <div id='cropSubmit' onClick={()=>this.setState({viewImage: false,cropOpen: true})}>DONE</div>
    </div>*/}
        {this.state.cropOpen&&<div id='cropWindow'>
          <p id="cropText">MAKE A FANCY SQUARE</p>
          <ReactCrop src={this.state.uploadedPic} crop={this.state.crop} onChange={this.onChange}/>
          <div id='cropSubmit' onClick={()=>this.getCroppedImg(this.state.uploadedPic,this.state.pixelCrop)}>FINISH CROP</div>
        </div>}
        {!this.state.cropOpen&&<div id="editContent">
          <div id="editPicture">
            <div id="picDiv">
              <ProfileImage source={this.state.profilePic}></ProfileImage>
              <div id="optionsDiv">
                <input id="changePic" className="fileInput" name="changePic" type="file" onChange={this.uploadPicture}></input>
                <label id="changePicLabel" htmlFor="changePic"><i className="far fa-edit"></i></label>
              </div>
          </div>
          </div>
          <div className="editRows" id="editContact">
            <div id="mailRow"><EditRow check={this.state.mailCheck} value={this.state.mail} label="YOUR MAIL" type="mail" name="mail" checkBox={true} onChange={this.handleInputChange}></EditRow></div>
            <div id="mailRow"><EditRow check={this.state.phoneCheck} value={this.state.phone} label="YOUR PHONE" type="number" name="phone" checkBox={true} onChange={this.handleInputChange}></EditRow></div>
          </div>
          <div className="editRows" id="editPersonal">
            <div id="mailRow"><EditRow check={this.state.mailCheck} value={this.state.location} label="LOCATION" type="text" name="location" checkBox={false} onChange={this.handleInputChange}></EditRow></div>

          </div>
          <div id="editMore">
            <p id="editDescP">PLAY STYLE (EDIT ONCE PER SEASON)</p>
            <div id="styleDiv">
             <div id="styleOne">

               <input value="1" onChange={this.handleInputChange} checked={this.state.mainSkill === "1"} name="mainSkill" id="styleCheck1" type="radio"/>
               <label id="sForehand" name="playStyle" className="checkLabel" htmlFor="styleCheck1"></label>
               <p>Strong Forehand</p>
               <input value="2" onChange={this.handleInputChange} checked={this.state.mainSkill === "2"} name="mainSkill" id="styleCheck2" type="radio"/>
               <label id="sBackhand" name="playStyle" className="checkLabel" htmlFor="styleCheck2"></label>
                <p>Strong Backhand</p>
               <input value="3" onChange={this.handleInputChange} checked={this.state.mainSkill === "3"} name="mainSkill" id="styleCheck3" type="radio"/>
               <label id="sBaseliner" name="playStyle" className="checkLabel" htmlFor="styleCheck3"></label>
               <p>Baseliner</p>
             </div>
              <div id="styleTwo">

               <input value="4" onChange={this.handleInputChange} checked={this.state.mainSkill === "4"} name="mainSkill" id="styleCheck4" type="radio"/>
               <label id="sServer" name="playStyle" className="checkLabel" htmlFor="styleCheck4"></label>
               <p>Serve & Volley</p>
               <input value="5" onChange={this.handleInputChange} checked={this.state.mainSkill=== "5"} name="mainSkill" id="styleCheck5" type="radio"/>
               <label id="sTactical" name="playStyle" className="checkLabel" htmlFor="styleCheck5"></label>
               <p>Tactical</p>
               <input value="6" onChange={this.handleInputChange} checked={this.state.mainSkill=== "6"} name="mainSkill" id="styleCheck6" type="radio"/>
              <label id="sAllround" name="playStyle" className="checkLabel" htmlFor="styleCheck6"></label>
               <p>All Round</p>
             </div>
            </div>
          </div>
          <div id="sliderDiv">
            <p>{this.state.lvlSlider/10}</p>
            <input step="5" onChange={this.handleInputChange} name="lvlSlider" type="range" min="0" max="70" value={this.state.lvlSlider} className="slider"></input>
          </div>
          <div onClick={this.handleSubmit} id="buttonContainer"><BlackButton content={"SUBMIT"}></BlackButton></div>
        </div>}
      </div>
    );
  }
}

export default EditProfile;
