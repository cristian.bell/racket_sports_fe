import React, {Component} from 'react';


class EditRow extends Component{
  constructor(props){
    super(props);
      this.state = {
    }

  }

  render() {
    const contStyle={
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    };
    const labelCol={
      width: "40%",

    };
    const inputCol={
      width: "55%",

    };
    const inputStyle={
      padding: "0",
      display: "block",
      margin: "0 auto",
      backgroundColor: "rgba(0,0,0,0)",
      border: "none",
      borderBottom: "1px solid grey",
      fontSize: '1rem'
    };
    const checkBox={
      width: "5%",
    };
    return (
      <div style={contStyle}>
        <div style={labelCol}>{this.props.label}</div>
        <div style={inputCol}>
          <input style={inputStyle}
                 type={this.props.type}
                 value={this.props.value}
                 name={this.props.name}
                 onChange={this.props.onChange}
                 readOnly={this.props.readOnly}
          />
        </div>
        <div style={checkBox}>{this.props.checkBox &&<input onChange={this.props.onChange}
                                                            checked={this.props.check}
                                                            name={this.props.name+"Check"}
                                                            type="checkBox"/>}</div>
      </div>
    );
  }
}

export default EditRow;
