import React, {Component} from 'react';
import './Profile.css';


class SkillDescDots extends Component{
  constructor(props){
    super(props);
    this.pointFillClass=["","","","",""];
    for(let i=0;i<this.props.fillDots;i++){
      
      this.pointFillClass[i]="filledPoint";
    }
      this.state = {

    }
  }


render() {
  return (
  <div className="pointChart" id="techChart">
    <div className={this.pointFillClass[0]}></div>
    <div className={this.pointFillClass[1]}></div>
    <div className={this.pointFillClass[2]}></div>
    <div className={this.pointFillClass[3]}></div>
    <div className={this.pointFillClass[4]}></div>
  </div>

);
}
}

export default SkillDescDots;
