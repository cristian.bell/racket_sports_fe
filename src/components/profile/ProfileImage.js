import React, {Component} from 'react';
import './Profile.css';

class ProfileImage extends Component{
  constructor(props){
    super(props);

      this.state = {
      colorOne: [255,148,148,1],
      colorTwo: [29,255,244,0.4],

      //google refuses to load local pictures so here have another cat for now!
    }
  }

  render() {
    const dynamicColor={
      backgroundImage: "linear-gradient(rgba("+this.state.colorOne.toString()+"), rgba("+this.state.colorTwo.toString()+"))"
    };
    const dynamicPicture={
      backgroundImage: `url( "${this.props.source}" )`
      };
    return (

      <div className="skillRing" style={dynamicColor}>
        <div className="profileImg" style={dynamicPicture}/>
      </div>

    );
  }
}

export default ProfileImage;
