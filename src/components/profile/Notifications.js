import React, {Component} from 'react';
import './Profile.css';
import NoteItem from './NoteItem';
import ReactTouchEvents from "react-touch-events";

class Notifications extends Component{
  constructor(props){
    super(props);
    this.handleSwipe=this.handleSwipe.bind(this);
      this.state = {
    }
  }

  handleSwipe (direction) {
    switch (direction) {
      case "top":
        console.log("top swipe: nothing yet");
        break;
      case "bottom":
        console.log("bottom swipe: nothing yet");
        break;
      case "left":
        this.props.swipeClose();
        break;
      case "right":this.props.swipeClose();
        break;
      default:
        break;
    }
  }

  render() {
    const animatedClass="notAnimated notificationMenu";

    const contStyle={
      height: "88%",
      width: "100%",
      position: "fixed", // Doesn't play nice with z-index
      top: "2.4rem",
      right: "0",
      backgroundImage: "linear-gradient(white 70%,#c5ff00)",
      backgroundPosition: "center",
      backgroundSize: "cover",
      zIndex: "2",
      overflow: "auto"
    };

    return (
      <ReactTouchEvents onSwipe={this.handleSwipe}>
      <div onSwipe={this.handleSwipe} className={animatedClass} style={contStyle}>
        <div style={{overflow: "scroll"}}>
         {this.props.notification.map((note, index)=> <NoteItem removeNote={this.props.removeNote} noteRedirectTo={this.props.noteRedirectTo} index={index} key={index} note={note}/>)}
        </div>
      </div>
      </ReactTouchEvents>
    );
  }
}

export default Notifications;
