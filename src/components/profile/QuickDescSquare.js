import React, {Component} from 'react';
import './UserProfile.css';
import allround from './allround.svg';
import baseliner from './baseliner.svg';
import strongForehand from './strongForehand.svg';
import strongBackhand from './strongBackhand.svg';
import tactical from './tactical.svg';
import serve from './serve.svg';

class QuickDesc extends Component{
  constructor(props){
    super(props);
      this.iconArr=[strongForehand,strongBackhand,baseliner,serve,tactical,allround];
      this.state = {
        picture: "https://i.imgur.com/Tgqh5Hn.jpg?1"
    }
  }

  render() {
    const CONTAINERSTYLE={
        height: '100%',
        width: '100%',
        fontFamily: "Open Sans, sans-serif !important",
        display: 'grid',
        gridTemplateColumns: '50% 50%',
        gridTemplateRows: '50% 50%',


      }
    const ITEMSTYLE={
      marginTop: "30%",
      color: "white",
      display: "flex",
      alignItems: "center",
      height: 0,
      paddingTop: "30%",
      paddingBottom: "30%",
      width: "60%",
      backgroundColor: "black",
      borderRadius: "50%"
    }
    const ONE={
      gridColumn: '1/2',
      gridRow: '1/2'
    }
    const TWO={
      gridColumn: '2/3',
      gridRow: '1/2'
    }
    const THREE={

    }
    const FOUR={

    }
    const ITEMPSTYLE={
      fontWeight: "bold",
      textAlign: "center",
      margin: "0 auto",
      fontSize: "1.2rem"
    }
    const ItemImgStyle={
      paddingTop: "50%",
      paddingBottom: "50%",
      width: "100%",
      borderRadius: "50%",
      backgroundImage: "url( "+this.iconArr[this.props.mainSkill]+" )",

      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat"
    }
    return (
      <div style={CONTAINERSTYLE}>
      <div style={ITEMSTYLE}>
        <p style={ITEMPSTYLE}>{this.props.playerSkill}</p>
      </div>
      <div style={ITEMSTYLE} >
        <p style={ITEMPSTYLE}>{this.props.dominantHand}</p>
      </div>
      <div style={ITEMSTYLE} >
        <p style={ITEMPSTYLE}>{this.props.backHand}</p>
      </div>
      <div style={ITEMSTYLE} >
        <div className="svgDiv" style={ItemImgStyle}></div>
      </div>
      </div>
    );
  }
}

export default QuickDesc;
