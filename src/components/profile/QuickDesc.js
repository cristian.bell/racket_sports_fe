import React, {Component} from 'react';
import './UserProfile.css';
import allround from './allround.svg';
import baseliner from './baseliner.svg';
import strongForehand from './strongForehand.svg';
import strongBackhand from './strongBackhand.svg';
import tactical from './tactical.svg';
import serve from './serve.svg';

class QuickDesc extends Component{
  constructor(props){
    super(props);
      this.iconArr=[strongForehand,strongBackhand,baseliner,serve,tactical,allround];
      this.state = {
        picture: "https://i.imgur.com/Tgqh5Hn.jpg?1"
    }
  }

  render() {
    const CONTAINERSTYLE={
        marginTop: "2%",
        marginBottom: "2%",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        fontFamily: "Open Sans, sans-serif !important"
      };
    const ITEMSTYLE={
      color: "white",
      display: "flex",
      alignItems: "center",
      height: 0,
      paddingTop: "11%",
      paddingBottom: "11%",
      width: "22%",
      backgroundColor: "black",
      borderRadius: "50%"
    };
    const ITEMPSTYLE={
      fontWeight: "bold",
      textAlign: "center",
      margin: "0 auto",
      fontSize: "120%"
    };
    const ItemImgStyle={
      paddingTop: "50%",
      paddingBottom: "50%",
      width: "100%",
      borderRadius: "50%",
      backgroundImage: "url( "+this.iconArr[this.props.mainSkill]+" )",

      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat"
    };
    return (
      <div style={CONTAINERSTYLE}>
      <div style={ITEMSTYLE}>
        <p style={ITEMPSTYLE}>{this.props.playerSkill}</p>
      </div>
      <div style={ITEMSTYLE} >
        <p style={ITEMPSTYLE}>{this.props.dominantHand}</p>
      </div>
      <div style={ITEMSTYLE} >
        <p style={ITEMPSTYLE}>{this.props.backHand}</p>
      </div>
      <div style={ITEMSTYLE} >
        <div className="svgDiv" style={ItemImgStyle}/>
      </div>
      </div>
    );
  }
}

export default QuickDesc;
