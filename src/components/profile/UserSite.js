import React, {Component} from 'react';
import { Redirect,Switch, Route} from 'react-router-dom';
import axios from 'axios';
import NavFooter from './NavFooter';
import './Profile.css';
import UserOptions from './UserOptions.js';
import Notifications from './Notifications.js';
import UserProfile from './UserProfile.js';
import Profile from './Profile.js';
import TournamentView from '../Tournaments/TournamentView.js';
import TournamentSum from '../Tournaments/TournamentSum.js';
import TournamentChart from '../Tournaments/TournamentChart.js';
import TournamentFeed from '../searchAndFeed/TournamentFeed.js';
import StatsView from './myStats/StatsView.js';
import Defs from '../Defs.js';
import myPic from '../../images/userIcon.png';
import EditProfile from './editProfile/EditProfile.js'
import SnackBar from '../helper/SnackBar.js';
import CreateTournament from '../createTournament/CreateTournament.js'

class UserSite extends Component{
  constructor(props){
    super(props);

    this.toggleOptions=this.toggleOptions.bind(this);
    this.toggleNotifications=this.toggleNotifications.bind(this);
    this.swipeClose=this.swipeClose.bind(this);
    this.letsLeave=this.letsLeave.bind(this);
    this.editProfile=this.editProfile.bind(this);
    this.removeNotification=this.removeNotification.bind(this);
    this.handleNav=this.handleNav.bind(this);
    this.uId=sessionStorage.getItem('MPuId');
    this.readNotes=[];
      this.state = {
      navIndex: 1,
      uId: this.uId,
      name : "John Doe",
      location: "BERLIN",
      profilePic: "",
      genderOption: ["MALE","FEMALE"],
      gender: "",
      age: "",
      email: "hello@hello.de",
      phone: "",
      estuff: "UPON REQUEST", //show email? can be just boolean
      mstuff: "UPON REQUEST", //show mobile? can be just bolean
      currentGear: "BABOLAT PURE DRIVE (2014 VER.)",
      gamesPlayed : 14,
      gamesWon : 9,
      dominantHandOption: [{desc: "RIGHT", short: "R"},{desc: "RIGHT", short: "R"},{desc: "LEFT", short: "L"}],
      dominantHand: {desc: "RIGHT", short: "R"},
      backHandOption: [{desc: "SINGLE", short: "S"},{desc: "SINGLE", short: "S"},{desc: "DOUBLE", short: "DB"}],
      backHand: {desc: "SINGLE", short: "S"},
      playFrequenceOption: ["NON FREQUENT","NON FREQUENT","FREQUENT PLAYER","VERY FREQUENT PLAYER","SERIOUS PLAYER"],
      playFrequence: "",
      experience : 6,
      playerSkill: "6.0",
      Pic: myPic,
      mainSkillOption:[{desc: "STRONG FOREHAND",short: "0"},{desc: "STRONG FOREHAND",short: "1"},{desc: "STRONG BACKHAND",short: "2"},{desc: "BASELINER",short: "3"},{desc: "SERVE",short: "4"},{desc: "TACTICAL PLAYER",short: "5"},{desc: "ALL ROUND",short: "6"}],  // object {skill: 1-6, imageOfSkill: url}
      mainSkill: {desc: "STRONG FOREHAND",short: "1"},

      character: 5,
      technique: 3,

      lastGameDate: "20.4.1889",

      seasonProgress: [5,3,4,9], // progress of season achievements 1-10 maybe people could unlock new achievements after filled up
      matchList: [{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spanda"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spanda"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"},{name:"K. Imamo",day:"monday",date: "1.1.2001",location: "berlin spandau"}],

      editProfile: false,
      userData: {},
      logOutMessage: "YOU HAVE BEEN LOGGED OUT!",
      loggedOut: false,
      optionToggle: false,
      notificationToggle: false,
      snackBarOpen: false,
      snackBarState: {message: "STANDART MESSAGE",state: 0,confirm: false},
      noteRedirectTo: {doRedirect: false,redirectLink: ''},
      notifications: [{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/home/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: '/editProfile'},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''},{id: "22",title: '', was_read: "false", message: "PLEASE LOG YOUR GAME ......1",src: '',redirect: ''}]
    }
  }
  handleSnackBar=(message, state, confirm, buttonContent, buttonAction)=>{
    console.log(message,state,confirm,buttonContent,buttonAction)
    this.setState({snackBarOpen: true, snackBarState: {message: message,state: state,confirm: confirm,buttonContent: buttonContent,buttonAction: buttonAction}})
    setTimeout(()=>this.setState({snackBarOpen: false}),5000)
  }
  removeNotification(index){
    const notes=this.state.notifications;
    //notes.splice(index,1);
    notes[index].read_on= Date.now();
    this.readNotes.push(notes[index])
    console.log(notes);
    console.log(this.readNotes)
    axios({
      method: 'post',
      //headers:{'x-api-key': '123'},
      url: Defs.be_url + 'notification/read/'+notes[index].id,
      withCredentials: true,
      //data: formData,
  }).then(response =>{
      console.log(response.data);
      if(response.status === 200){
        console.log("succes 200")
      }
  }).catch(response =>{
      //handle error
      console.log(response);
      
  });
    
  }
  
  
  
  /*static getDataSafe(inputData,defaultData){
    try{
      return inputData
    }catch(e){
      return defaultData
    }
  }*/
  
  /*routerBullshit(testInput){
    if(testInput){
      return this.props.location.state.reference
    }
    else return 'something went wrong'
  }
  */
  componentDidMount(){
    if(sessionStorage.hasOwnProperty('MPuId')) {
      this.uId=sessionStorage.getItem('MPuId');

      if (typeof this.uId === 'string' && this.uId !== null) {
        axios.all([axios.get(Defs.be_url + "user/" + this.uId),
          axios.get(Defs.be_url + "notification/list/unread/", {withCredentials:true})])
            .then(axios.spread((response, notifications) =>{
              const userData=response.data;
              const notes=notifications.data;
              this.setState({userData});
              console.log(userData)
              this.setState({
                name: this.state.userData.firstname+" "+this.state.userData.lastname,
                location: this.state.userData.city+" "+this.state.userData.country,
                playerSkill: this.state.userData.level,
                dominantHand: this.state.dominantHandOption[this.state.userData.dominant_hand],
                backHand: this.state.backHandOption[this.state.userData.dominant_hand],
                experience: this.state.userData.play_years,
                playFrequence: this.state.playFrequenceOption[this.state.userData.play_times],
                gamesPlayed: this.state.userData.gamesPlayed,
                gamesWon: this.state.userData.gamesWon,
                mainSkill: this.state.mainSkillOption[this.state.userData.main_skill],
                lastGameDate: this.state.userData.lastGameDate,
                currentGear: this.state.userData.gear_1,
                gender: this.state.genderOption[this.state.userData.gender],
                age: this.state.userData.age,
                email: this.state.userData.email,
                phone: this.state.userData.phone,
                profilePic: response.data.photo,
                notifications: notes
              })
              //console.log(typeof(notes))
              console.log(this.state.notifications)
              //console.log(typeof(this.state.notifications))
            }))
            .catch(response =>{
              console.log(response)
            });
      }
    }
  }
/* noteRedirectTo=(index)=>{
  let notecopy=this.state.notifications;
  notecopy[index].was_read=true;
  if(notecopy[index].redirect.length>5){
    this.setState({noteRedirectTo:{doRedirect: true,redirectLink: this.state.notifications[index].redirect},notifications: notecopy})

  }
}
goToNoteLink=(link)=>{
  return(<Redirect to={{
    pathname: link,
    state: { reference:this.state}
}} />);
} */
letsLeave() {
  axios.post(Defs.be_url + "user/logout", {}, {withCredentials:true})
      .then(response=>{
        console.log(response);
        /*if(response.status === 200){
          this.props.logMeOut();
        }*/
      })
      .catch(response=>{
        console.log(response);
      })
      .finally(response=>{
        this.props.logMeOut();
      });
}

toggleOptions(event){
  event.preventDefault();
  this.setState({
    optionToggle: !this.state.optionToggle,
    notificationToggle: false,
  })
}
toggleNotifications(event){
  event.preventDefault();

  this.setState({
    notificationToggle: !this.state.notificationToggle,
    optionToggle: false
  })
}
swipeClose(){
  this.setState({
    optionToggle: false,
    notificationToggle: false
  })
}
editProfile(){
  this.setState({
    editProfile: true
  })
}
handleNav(i){
  this.setState({
    navIndex: i
  })
}
handleSwipe (direction) {
  switch (direction) {
    case "top": console.log("nothing yet");
    break;
    case "bottom": console.log("nothing yet");
    break;
    case "left": this.setState({
      optionToggle: false,
      notificationToggle: false
    });
    break;
    case "right":this.setState({
      optionToggle: false,
      notificationToggle: false
    });
    break;
    default:
      break;
  }
    }
  render() {
    /* if(this.state.noteRedirectTo.doRedirect){
      return this.goToNoteLink(this.state.noteRedirectTo.redirectLink)
    } */
    //console.log(this.props.location.state.reference)
    /* if(this.state.editProfile) return(<Redirect to={{
              pathname: '/home/editProfile',
              //state: { reference:this.state}
          }} />);
     *///prevent logout// @todo remove this..
    if(this.state.loggedOut) return(<Redirect to={{
              pathname: '/LogIn',
              state: { reference: this.state.logOutMessage }
          }} />);
    return (
      <div style={{height: window.innerHeight +"px", overflow: "hidden" /*, zIndex: "4"*/}} className={"profileComp"}>
        {/*style={{zIndex: "3"}}*/}
        <div id="topNav">
          <nav id="profileNav">
            <div onClick={this.toggleOptions}  className="appIcon">
                <div id="hitPointIcon"/>
                <div id="menuArrow" className={this.state.optionToggle ? 'menuOpen' : ''}/>
            </div>
            <div id="settings"/>
            <div onClick={this.toggleNotifications} id="notifications">{this.state.notifications.length>0 && <div id="noteInfo"/>}<i className="fas fa-bell"/></div>

          </nav>
        </div>
        {this.state.optionToggle && <UserOptions editProfile={this.editProfile} logOut={this.letsLeave} swipeClose={this.swipeClose.bind(this)}/>}
        {this.state.notificationToggle && <Notifications noteRedirectTo={this.noteRedirectTo} removeNote={this.removeNotification}  notification={this.state.notifications} swipeClose={this.swipeClose.bind(this)}/>}

      {/*<div style={{marginTop: "-4%", zIndex: "1",height: "100%"}}  className="">*/}
      
      <div style={{zIndex: "1",height: "88%", overflow: 'auto'}} className="" /*THIS PART SCROLLS */>
        <Switch>
          <Route exact path='/home' render={(props)=><UserProfile handleSnack={this.handleSnackBar} navIndex={this.handleNav} uId={this.uId} {...this.props}/>}/>
          <Route path='/home/TournamentFeed' render={(props)=><TournamentFeed closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} navIndex={this.handleNav} userObject={this.state} uId={this.uId} {...this.props}/>}/>
          <Route path='/home/EditProfile'render={(props)=><EditProfile handleSnack={this.handleSnackBar} closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} userObject={this.state} {...this.props}/>}/>
          <Route path='/home/TournamentSum'  render={(props)=><TournamentSum closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} userObject={this.state} {...this.state}/>}/>
          <Route path='/home/TournamentView/:id' render={(props)=><TournamentView handleSnack={this.handleSnackBar} closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} userObject={this.state} {...props}/>}/>
          <Route path='/home/StatsView' render={(props)=><StatsView closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} userObject={this.state} navIndex={this.handleNav} {...this.props}/>}/>
          <Route path='/home/Profile/:id' render={(props)=><Profile {...props}/>}/>
          <Route path='/home/TournamentChart' render={(props)=><TournamentChart closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} {...this.state}/>}/>
          <Route path='/home/CreateTournament' render={(props)=><CreateTournament handleSnack={this.handleSnackBar} closeOptions={()=>this.setState({optionToggle: false,notificationToggle: false})} {...this.state}/>}/>
        </Switch>

        {this.state.snackBarOpen&&<div /*onClick={()=>this.setState({snackBarOpen: false})}*/ id='snackBarDiv'>
        <SnackBar message={this.state.snackBarState.message} 
        withConfirm={this.state.snackBarState.confirm} 
        state={this.state.snackBarState.state} 
        buttonContent={this.state.snackBarState.buttonContent}
        actionHandler={this.state.snackBarState.buttonAction}>
        </SnackBar></div>}

      </div>

      <div style={{zIndex: "2"}} id="navFootUser"><NavFooter navIndex={this.state.navIndex}/></div>
      </div>
    );
  }
}

export default UserSite;
