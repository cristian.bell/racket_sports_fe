import React from 'react';
import './StoreTab.css';


const StoreTab = (props) =>{
    return(
    <div className="storeContainer">
      <div className="mainSponsore">
        <div className="sponsoreIcon"/>
        <div className="sponsoreText"/>
      </div>
      <div className="secondSponsore">
        <div className="sponsoreIcon"/>
        <div className="sponsoreText"/>
      </div>
      <div className="thirdSponsore">
        <div className="sponsoreIcon"/>
        <div className="sponsoreText"/>
      </div>
      <div className="forthSponsore">
        <div className="sponsoreIcon"/>
        <div className="sponsoreText"/>
      </div>
    </div>
  );
  }

export default StoreTab;
