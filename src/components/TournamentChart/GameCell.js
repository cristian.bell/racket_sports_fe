import React, {Component} from 'react';
import Player from './Player.js';
import './DynamicChart.css';

class GameCell extends Component{
  constructor(props){
      super(props);
      this.animateMargin=this.animateMargin.bind(this);
       this.state = {
         changeMargin: "startAnimation withMargin"
    }
}
animateMargin(){
  if(this.state.changeMargin === "startAnimation withMargin"){
    this.setState({
      changeMargin: "startAnimation noMargin"
    })
  }else{
    this.setState({
      changeMargin: "startAnimation withMargin"
    })
  }
}

render() {
  const gameCell={
    height: "12vw",
    width: "100%",
  };

    return (
    <div onClick={this.props.animateMargin} className={this.props.changeMargin} style={gameCell}>
      <div className="playerContainer" ref={this.props.getRef}>
        <Player game={this.props.game.one} player={0}/>
        <Player game={this.props.game.two} player={1}/>
      </div>
    </div>
    );
  }
}

export default GameCell;
