import React, {Component} from 'react';
import './DynamicChart.css';

class EmptyCell extends Component{
  constructor(props){
      super(props);
      this.animateMargin=this.animateMargin.bind(this);
       this.state = {
         changeMargin: "startAnimation withMargin"
    }
}
animateMargin(){
  if(this.state.changeMargin === "startAnimation withMargin"){
    this.setState({
      changeMargin: "startAnimation noMargin"
    })
  }else{
    this.setState({
      changeMargin: "startAnimation withMargin"
    })
  }
}

render() {
  const gameCell={
    height: "12vw",
    width: "100%",
  }

    return (
    <div onClick={this.props.animateMargin} className={this.props.changeMargin} style={gameCell}>
    </div>
    );
  }
}

export default EmptyCell;
