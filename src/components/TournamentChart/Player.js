import React, {Component} from 'react';

class Player extends Component{
  constructor(props){
      super(props);
       this.state = {
        color: [[200,100,200,0.8],[70,100,100,0.8]]
    }
}
// src/components/profile/profileTestImage.jpg
render() {
  const gameItem={
    width: "100%",
    height: "44%",
    marginBottom: "2%",
    marginTop: "2%",
    outline: "2px solid white",
    display: "flex",
    backgroundColor: "rgba("+this.state.color[this.props.player]+")",
    alignItems: "center"
  };
  const imgCell={

  };
  const nameCell={
    marginLeft: "6%",
    height: "100%",
    width: "80%",
  };

    return (
    <div style={gameItem}>
      <div style={imgCell} className="imgCellClass"/>
      <div style={nameCell}>{this.props.game.name}</div>
      <div className="scoreCellClass">{this.props.game.score}</div>
    </div>
    );
  }
}

export default Player;
