import React, {Component} from 'react';
import GameCell from './GameCell.js'
import './DynamicChart.css';


class DynamicChart extends Component{
  constructor(props){
      super(props);
      this.shrinkIt=this.shrinkIt.bind(this);
      this.animateMargin=this.animateMargin.bind(this);
      this.pushReference=this.pushReference.bind(this);
      this.generateBracket=this.generateBracket.bind(this);
      this.gameRef=[];
      this.testArray=new Array(13);
      this.index=0;
      this.tournamentBracket=this.generateBracket(this.testArray);
       this.state = {
         gap: 20,
         changeMargin: "startAnimation withMargin",
         connectionTop: "100px",
         connectionHeight: "60px",
         connectLeft: "200px",
         game: {one:{name: "One",picture:"XX",score: "20" },two:{name: "Two",picture: "XX",score: "40" },winner:{name: "Two",picture: "XX",score: "40" }}
    }
}
shrinkIt(){
  if(this.state.gap>2){
    this.setState({
      gap: 0
    })
  }else{
    this.setState({
      gap: 20
    })
  }

}
animateMargin(){

  if(typeof this.state.changeMargin === 'string' && this.state.changeMargin === "startAnimation withMargin"){
    this.setState({
      changeMargin: "startAnimation noMargin"
    })
  }else{
    this.setState({
      changeMargin: "startAnimation withMargin"

    })
  }
}

  pushReference(event){
    this.myHeight=0;
    this.myHeight=event.getBoundingClientRect();
    this.gameRef.push(this.myHeight.top+this.myHeight.height/2);
    console.log(this.myHeight);
    console.log(this.gameRef);

  }
generateBracket(teams){
  this.bracketArr=[];

  this.count=teams.length;
  console.log(this.count);
  if(this.count<=4){
    return 0
  }
  for(this.powerIndex=0;(this.count/Math.pow(2,this.powerIndex))>=1;this.powerIndex++){
    this.powerOfTwo=(Math.floor(Math.pow(2,this.powerIndex)));
    this.bracketArr.push(new Array(this.powerOfTwo));
    console.log(this.powerOfTwo)
    // fill base games with gameObject
    for(this.i=0;this.i<this.bracketArr.length;this.i++){
      for(this.j=0;this.j<this.bracketArr[this.i].length;this.j++){
        this.bracketArr[this.i][this.j]={one:{name: "One",picture:"XX",score: "20" },two:{name: "Two",picture: "XX",score: "40" }};
      }
    }
    if(this.count/this.powerOfTwo<2){
       // create pregames length-1
      this.bracketArr.push(new Array(this.count-this.powerOfTwo));
      for(this.i=0;this.i<(this.count-this.powerOfTwo);this.i++){
        // connect pregames to first game week
        this.bracketArr[this.bracketArr.length-1][this.i]={one:{name: "One",picture:"XX",score: "20" },two:{name: "Two",picture: "XX",score: "40" },winner:{name: "W:"+this.i,picture: "XX",score: "0" }};
        this.bracketArr[this.bracketArr.length-2][this.i].one=this.bracketArr[this.bracketArr.length-1][this.i].winner;
      }
      //return this.count%this.powerOfTwo
    }

  }
  console.log(this.bracketArr);
  this.bracketArr.reverse();
  return(this.bracketArr)
}
printGames=()=>{
  for (this.i=0; this.i < this.generateBracket(this.testArray); this.i++) {
    return(<GameCell game={this.state.game} getRef={this.pushReference} animateMargin={this.animateMargin} changeMargin={this.state.changeMargin}/>)
  }
};
render() {
    return (
    <div onClick={this.printGames} id="topContainer">
    <div  id="tournamentDiv">
    {this.tournamentBracket.map(row =><div ref="singleRef" className="gameCol">
        <div className="gamesDiv">
          {row.map(col=><GameCell game={col} getRef={this.pushReference} animateMargin={this.animateMargin} changeMargin={this.state.changeMargin}/>)}
        </div>
       <div className="connection">
      </div>
    </div>)}
    </div>
    </div>
    );
  }
}

export default DynamicChart;

