import {Component} from "react";
import React from "react";
import Defs from "./Defs";
import * as EmailValidator from "email-validator";

class DefFunctions extends Component {
    constructor(props){
        super();
    }

    levelToFloat(level) {
        if (parseInt(level) === parseFloat(level))
            level += '.0';
        return level;
    }

    checkFormDataStep1(formData, parentContext) {
        console.debug(formData);
        Defs.formError = true;

        if (formData.firstName.length < Defs.firstname_min_len) {
            parentContext.setState({
                fieldError: true,
            });
            Defs.formErrorMessage = "FIRST NAME IS TOO SHORT";
            return false;
        }

        if (formData.email.length < Defs.email_min_len) {
            parentContext.setState({
                fieldError: true,
                fieldMessage1: "E-Mail address IS TOO SHORT OR INCORRECT"
            });
            Defs.formErrorMessage = "E-Mail address IS TOO SHORT OR INCORRECT";
            return false;
        } else {
            if (EmailValidator.validate(formData.email) !== true) {
                parentContext.setState({
                    fieldError: true,
                    fieldMessage1: "E-Mail address IS incorrect"
                });
                Defs.formErrorMessage = "E-Mail address IS incorrect";
                return false;
            }
        }

        if (formData.password.length >= Defs.pwd_min_len) {
            if (formData.password === formData.passwordRe) {
                parentContext.setState({
                    signUpFinished1: true,
                    fieldError: false
                });
            } else {
                parentContext.setState({
                    fieldError: true,
                    fieldMessage1: "PASSWORD DO NOT MATCH"
                });
                Defs.formErrorMessage = "PASSWORDs DO NOT MATCH";
                return false;
            }
        } else {
            parentContext.setState({
                fieldError: true,
                fieldMessage1: 'PASSWORD IS TOO SHORT (min length: ' + Defs.pwd_min_len + ')'
            });
            Defs.formErrorMessage = 'PASSWORD IS TOO SHORT (min length: ' + Defs.pwd_min_len + ')';
            return false;
        }

        Defs.formError = false;
        return true;
    }

    checkFormDataStep2(formData, parentContext) {
        Defs.formError = true;

        if (formData.gender == null) {
            parentContext.setState({
                fieldError: true,
            });
            Defs.formErrorMessage = "please select a gender";
            return false;
        }
        if (formData.ageGroup == null) {
            parentContext.setState({
                fieldError: true,
                fieldMessage: "please select an age group"
            });
            Defs.formErrorMessage = "please select an age group";
            return false;
        }

        Defs.formErrorMessage = '';
        Defs.formError = false;
        return true;
    }

    checkFormDataStep3(formData, parentContext) {
        if (formData.playFrequence == null) {
            parentContext.setState({
                fieldError: true,
                fieldMessage: "please select the times you play"
            });
            Defs.formErrorMessage = "please select the times you play";
            return false;
        }
        if (formData.playExp == null) {
            parentContext.setState({
                fieldError: true,
                fieldMessage: "please select playing years"
            });
            Defs.formErrorMessage = "please select playing years";
            return false;
        }
        if (formData.mainSkill == null || formData.mainSkill2 == null) {
            parentContext.setState({
                fieldError: true,
                fieldMessage: "please select your play style"
            });
            Defs.formErrorMessage = "please select your play style (choose 2)";
            return false;
        }

        Defs.formErrorMessage = '';
        Defs.formError = false;
        return true;
    }

    checkFormData(formData) {
        console.debug(formData);
    }
}

export default DefFunctions;