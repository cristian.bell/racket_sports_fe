import React, {Component} from 'react';
import Main from './components/Main';
import './App.css';

class App extends Component{
  constructor(props){
    //console.log("App() called");
    super(props);
    this.state = {
      isLoggedIn: false
    }
}
componentDidMount(){
  window.addEventListener('beforeunload',()=>sessionStorage.setItem('isLoggedIn', JSON.stringify(this.state.isLoggedIn)))
  if(sessionStorage.hasOwnProperty('isLoggedIn')){
    this.setState({isLoggedIn: JSON.parse(sessionStorage.getItem('isLoggedIn'))},
    ()=>this.clearSessionStorage(this.state.isLoggedIn)
    )
  }
}
clearSessionStorage=(logInState)=>{
  console.log(logInState)
  if(!logInState){
    if(sessionStorage.hasOwnProperty('isLoggedIn')){
      sessionStorage.removeItem('isLoggedIn')
    }
    if(sessionStorage.hasOwnProperty('MPuId')){
      sessionStorage.removeItem('MPuId')
    }
  }
}
handleLogIn(event){
  this.setState({
    isLoggedIn: true
  });
  
}

handleLogOut(event){
  this.setState({
    isLoggedIn: false
  },
  ()=>this.clearSessionStorage(this.state.isLoggedIn)
  );
}
render(){
      console.log('App isLoggedIn: ' + this.state.isLoggedIn);
  return (
    <div className="App">
      <Main logInState={this.state.isLoggedIn} logIn={this.handleLogIn.bind(this)} logOut={this.handleLogOut.bind(this)}/>
    </div>
    );
  }
}
export default App;
